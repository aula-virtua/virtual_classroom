<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
    Route::group(['prefix' => 'n', 'namespace' => 'Public'], function () {
    });

    Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
        Route::post('/register', 'RegisterStudentController@register');
        Route::post('/login', 'LoginController@login');
        Route::post('/forgot', 'ForgotPasswordController@forgot');
        Route::post('/reset', 'ForgotPasswordController@reset');
        Route::get('/verify/{token}', 'ActivationController@verify');
    });

    Route::group(['namespace' => 'Session'], function () {
        Route::group(['prefix' => 'a', 'namespace' => 'Administrator', 'middleware' => 'api.auth:administrator'], function () {
            Route::get('/teacher', 'TeacherController@index');
            Route::get('/teacher/{code}', 'TeacherController@show');
            Route::post('/teacher', 'TeacherController@store');
            Route::put('/teacher/{code}', 'TeacherController@update');
            Route::delete('/teacher/{code}', 'TeacherController@destroy');

            Route::get('/student', 'StudentController@index');
            Route::put('/student/{code}', 'StudentController@update');
            Route::delete('/student/{code}', 'StudentController@destroy');
        });

        Route::group(['prefix' => 't', 'namespace' => 'Teacher', 'middleware' => 'api.auth:teacher'], function () {
            Route::get('/course', 'CourseController@index');
            Route::post('/course', 'CourseController@store');
            Route::group(['middleware' => 'teacher.course'], function () {
                Route::get('/course/{id}', 'CourseController@show');
                Route::get('/course/{id}/student', 'StudentController@index');
                Route::put('/course/{id}', 'CourseController@update');
                Route::delete('/course/{id}', 'CourseController@destroy');

                Route::get('/course/{id}/homework', 'HomeworkController@index');
                Route::post('/course/{id}/homework', 'HomeworkController@store');
                Route::group(['middleware' => 'teacher.homework'], function () {
                    Route::get('/course/{id}/homework/{hid}', 'HomeworkController@show');
                    Route::post('/course/{id}/homework/{hid}', 'HomeworkController@update');
                    Route::delete('/course/{id}/homework/{hid}', 'HomeworkController@destroy');

                    Route::group(['middleware' => 'teacher.student'], function () {
                        Route::get('/course/{id}/homework/{hid}/student/{code}', 'StudentController@show');
                        Route::put('/course/{id}/homework/{hid}/student/{code}', 'StudentController@update');
                    });
                });

                Route::post('/course/{id}/forum', 'ForumController@store');
                Route::group(['middleware' => 'teacher.forum'], function () {
                    Route::put('/course/{id}/forum/{fid}', 'ForumController@update');
                    Route::delete('/course/{id}/forum/{fid}', 'ForumController@destroy');
                });
            });
        });

        Route::group(['prefix' => 's', 'namespace' => 'Student', 'middleware' => 'api.auth:student'], function () {
            Route::get('/course/join/{code}', 'JoinController@join');
            Route::delete('/course/join/{id}', 'JoinController@leave');

            Route::get('/course', 'CourseController@index');
            Route::group(['middleware' => 'student.course'], function () {
                Route::get('/course/{id}', 'CourseController@show');
                Route::get('/course/{id}/homework', 'HomeworkController@index');
                Route::group(['middleware' => 'student.homework'], function () {
                    Route::get('/course/{id}/homework/{hid}', 'HomeworkController@show');
                    Route::post('/course/{id}/homework/{hid}', 'HomeworkController@store');
                });
            });
        });

        Route::group(['prefix' => 'sh', 'namespace' => 'Shared', 'middleware' => 'api.auth:administrator|teacher|student'], function () {
            Route::get('/auth', 'ProfileController@index');
            Route::post('/auth', 'ProfileController@update');
            Route::get('/auth/logout', 'ProfileController@logout');
            Route::post('/auth/password', 'ProfileController@updatePassword');

            Route::get('/resource', 'ResourceController@index')->middleware('shared.resource');

            Route::group(['middleware' => 'shared.course'], function () {
                Route::get('/course/{id}/forum', 'ForumController@index');
                Route::group(['middleware' => 'shared.forum'], function () {
                    Route::get('/course/{id}/forum/{fid}/comment/{cid?}', 'CommentController@index');
                    Route::post('/course/{id}/forum/{fid}/comment/{cid?}', 'CommentController@store');
                    Route::group(['middleware' => 'shared.comment'], function () {
                        Route::put('/course/{id}/forum/{fid}/comment/{cid}', 'CommentController@update');
                        Route::delete('/course/{id}/forum/{fid}/comment/{cid}', 'CommentController@destroy');
                    });
                });
            });
        });
    });
});

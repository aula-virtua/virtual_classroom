<?php

namespace Tests\Support;

use App\Http\Controllers\Shared\Constants\UserType;
use App\Models\User;
use Illuminate\Foundation\Testing\Concerns\MakesHttpRequests;
use MainUsersSeeder;

/**
 * @author Eduardo Fuentes <eduardo.fuentes.rangel@gmail.com>
 */
trait Authenticate
{
    use MakesHttpRequests;

    /**
     * Token from the response of the server.
     *
     * @var string
     */
    protected $token;

    /**
     * Sign in a user into the server with the given credentials.
     *
     * @param string|int $code
     * @param string     $password
     * @param string     $ip
     *
     * @return TestResponse
     */
    public function signIn($code, string $password, string $ip)
    {
        return $this->json('POST', url('v1/auth/login'), [
            'code'     => $code,
            'password' => $password
        ], [], [], [
            'REMOTE_ADDR'  => $ip,
            'Content-Type' => 'application/x-www-url-encoded'
        ]);
    }

    /**
     * Sign into the server with the given role
     *
     * @param int      $userType One value from the constansts defined in `UserType`
     * @param string   $ip
     *
     * @return $this
     */
    public function signInAs(int $userType, string $ip = '127.0.0.1')
    {
        $type = UserType::toString($userType);

        $code = MainUsersSeeder::$users[$type]['code'];
        $password = MainUsersSeeder::$users[$type]['password'];

        $response = $this->signIn($code, $password, $ip);

        $body = $response->getOriginalContent();
        $this->token = $body['token'];

        return $this->withToken();
    }

    /**
     * Allow to sign in to the server with a `User` instance. The password by
     * default is the one use in the `UserFactory`.
     *
     * @param User   $user      User to sign in with
     * @param string $password  Password for the given user
     * @param string $ip        Custom IP Address, by default localhost
     *
     * @return $this
     */
    public function signInWith(
        User $user,
        string $password = '12345',
        string $ip = '127.0.0.1'
    ) {
        $response = $this->signIn($user->code, $password, $ip);
        $body = $response->getOriginalContent();
        $this->token = $body['token'];

        return $this->withToken();
    }

    /**
     * Append a token for a request.
     *
     * @param string $token
     *
     * @return $this
     */
    public function withToken(array $headers = [])
    {
        $token = $this->token;

        return $this->withHeaders(
            array_merge($headers, [
                'Authorization' => "Bearer ${token}"
            ])
        );
    }
}

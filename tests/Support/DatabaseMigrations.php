<?php

namespace Tests\Support;

trait DatabaseMigrations
{
    protected static $migrated = false;

    /**
     * Define hooks to migrate the database before and after each test.
     *
     * @return void
     */
    public function runDatabaseMigrations()
    {
        var_dump('migrations');
        if (!static::$migrated) {
            $this->artisan('migrate:fresh');

            static::$migrated = true;
        }
    }
}

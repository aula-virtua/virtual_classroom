<?php

namespace Tests\Support;

use App\Http\Controllers\Shared\Constants\UserType;
use Tests\TestCase;

/**
 * @author Eduardo Fuentes <eduardo.fuentes.rangel@gmail.com>
 */
abstract class TeacherAuthenticate extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->signInAs(UserType::TEACHER);
    }
}

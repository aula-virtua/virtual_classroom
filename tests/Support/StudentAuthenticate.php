<?php

namespace Tests\Support;

use App\Http\Controllers\Shared\Constants\UserType;
use Tests\TestCase;

abstract class StudentAuthenticate extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->signInAs(UserType::STUDENT);
    }
}

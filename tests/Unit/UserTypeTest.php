<?php

namespace Tests\Unit;

use App\Http\Controllers\Shared\Constants\UserType;
use PHPUnit\Framework\TestCase;

/**
 * @author Eduardo Fuentes <eduardo.fuentes.rangel@gmail.com>
 */
class UserTypeTest extends TestCase
{
    public function testUserTypesToString()
    {
        $this->assertEquals(UserType::toString(UserType::ADMINISTRATOR), 'administrator');
        $this->assertEquals(UserType::toString(UserType::TEACHER), 'teacher');
        $this->assertEquals(UserType::toString(UserType::STUDENT), 'student');
        $this->assertEquals(UserType::toString(4), null);

        $this->assertNotEquals(UserType::toString(2), 'administrator');
        $this->assertNotEquals(UserType::toString(3), 'administrator');
        $this->assertNotEquals(UserType::toString(3), 'teacher');
        $this->assertNotEquals(UserType::toString(1), 'student');
    }
}

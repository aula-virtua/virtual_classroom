<?php

namespace Tests;

use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\Support\Authenticate;
use Tymon\JWTAuth\Facades\JWTAuth;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, Authenticate;

    protected static $migrated = false;

    /**
     * Faker Instance to use in the test
     *
     * @var Generator
     */
    protected $faker;

    public function setUp(): void
    {
        parent::setUp();

        if (!static::$migrated) {
            $this->artisan('migrate:fresh');
            $this->artisan('db:seed');

            static::$migrated = true;
        }

        $this->faker = Factory::create();
    }

    protected function assertToken(string $token)
    {
        $payload = JWTAuth::setToken($token)
            ->getPayload()->toArray();

        (new TestResponse(response()->json($payload)))
            ->assertJsonStructure([
                'iss',
                'iat',
                'exp',
                'nbf',
                'sub',
                'jti',
                'typ',
            ]);
    }
}

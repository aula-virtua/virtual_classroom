<?php

namespace Tests\Feature\Session\Student;

use App\Models\Course;
use App\Models\Homework;
use MainUsersSeeder;
use Tests\Support\StudentAuthenticate;

class CourseTest extends StudentAuthenticate
{
    public function testShouldRetrieveCourses()
    {
        $code = MainUsersSeeder::$users['student']['code'];
        $courses = factory(Course::class, 5)->create();
        $courses->each(function ($c) use ($code) {
            $c->students()->attach($code);
            factory(Homework::class, 4)->create([
                'course_id' => $c->id
            ]);
        });

        $this->withToken()
            ->get(url('v1/s/course'))
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    [
                        'id',
                        'name',
                        'description',
                        'banner',
                        'createdBy' => [
                            'code',
                            'name',
                            'firstLastName',
                            'secondLastName',
                            'email',
                        ],
                        'difference',
                    ]
                ],
                'meta',
                'links' => [
                    'first',
                    'last',
                    'next',
                    'prev'
                ]
            ]);
    }

    public function testShouldRetrieveCourse()
    {
        $code = MainUsersSeeder::$users['student']['code'];
        $course = factory(Course::class)->create();
        $course->students()->attach($code);

        $this->withToken()
            ->get(url('v1/s/course/' . $course->id))
            ->assertOk()
            ->assertJsonStructure([
                'id',
                'name',
                'description',
                'banner',
                'createdBy' => [
                    'code',
                    'name',
                    'firstLastName',
                    'secondLastName',
                    'email',
                ],
                'difference',
                'homeworks',
                'forums',
                'calification'

            ]);
    }
}

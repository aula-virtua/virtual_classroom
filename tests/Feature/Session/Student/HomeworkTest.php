<?php

namespace Tests\Feature\Session\Student;

use App\Models\Course;
use App\Models\Homework;
use App\Models\HomeworkFile;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use MainUsersSeeder;
use Tests\Support\StudentAuthenticate;

class HomeworkTest extends StudentAuthenticate
{
    public function testShouldRetrieveHomeworks()
    {
        $code = MainUsersSeeder::$users['student']['code'];
        $course = factory(Course::class)->create();
        $course->students()->attach($code);
        factory(Homework::class, 4)->create([
            'course_id' => $course->id
        ]);

        $this->withToken()
            ->get(url('v1/s/course/' . $course->id . '/homework'))
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    [
                        'id',
                        'name',
                        'description',
                        'deliveryDate',
                        'createdAt',
                        'updatedAt',
                        'deletedAt'
                    ]
                ],
                'meta',
                'links' => [
                    'first',
                    'last',
                    'prev',
                    'next'
                ]
            ]);
    }

    public function testShouldRetrieveHomework()
    {
        $code = MainUsersSeeder::$users['student']['code'];
        $teacherCode = MainUsersSeeder::$users['teacher']['code'];
        $course = factory(Course::class)->create();
        $course->students()->attach($code);
        $homework = factory(Homework::class)->create([
            'course_id' => $course->id
        ]);
        factory(HomeworkFile::class, 5)->create([
            'homework_id' => $homework->id,
            'user_code'   => $teacherCode
        ]);

        $this->withToken()
            ->get(url('v1/s/course/' . $course->id . '/homework/' . $homework->id))
            ->assertOk()
            ->assertJsonStructure([
                'id',
                'name',
                'description',
                'maxCalification',
                'upload',
                'files' => [
                    [
                        'fileName',
                        'originalName',
                        'mimeType'
                    ]
                ],
                'createdAt',
                'updatedAt',
                'deletedAt',
            ]);
    }

    public function testShouldUploadFilesToHomework()
    {
        Storage::fake('homework_files');
        $code = MainUsersSeeder::$users['student']['code'];
        $teacherCode = MainUsersSeeder::$users['teacher']['code'];
        $course = factory(Course::class)->create();
        $course->students()->attach($code);
        $homework = factory(Homework::class)->create([
            'course_id' => $course->id
        ]);
        factory(HomeworkFile::class, 5)->create([
            'homework_id' => $homework->id,
            'user_code'   => $teacherCode
        ]);

        $data = [
            'files' => [
                UploadedFile::fake()->image('avatar_1.png'),
                UploadedFile::fake()->image('avatar_2.png'),
            ]
        ];

        $response = $this->withToken()
            ->post(url('v1/s/course/' . $course->id . '/homework/' . $homework->id), $data);

        $response->assertOk()
            ->assertJsonStructure([
                'id',
                'name',
                'description',
                'maxCalification',
                'upload' => [
                    'calification',
                    'files' => [
                        [
                            'fileName',
                            'originalName',
                            'mimeType'
                        ]
                    ],
                ],
                'files' => [
                    [
                        'fileName',
                        'originalName',
                        'mimeType'
                    ]
                ],
                'createdAt',
                'updatedAt',
                'deletedAt',
            ]);


        $body = $response->getOriginalContent();
        $files = $body['upload']['files'];

        foreach ($files as $file) {
            Storage::disk('homework_files')->assertExists(
                sprintf('%s/%s/%s', $course->id, $homework->id, $code),
                $file['fileName']
            );
        }
    }
}

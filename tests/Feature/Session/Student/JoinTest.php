<?php

namespace Tests\Feature\Session\Student;

use App\Models\Course;
use App\Models\User;
use Tests\TestCase;

class JoinTest extends TestCase
{
    public function testShouldJoinCourse()
    {
        $user = factory(User::class)->create();
        $course = factory(Course::class)->create();

        $this->signInWith($user)
            ->get(url('v1/s/course/join/' . $course->code))
            ->assertOk()
            ->assertJson([
                'success' => true
            ]);

        $this->assertDatabaseHas('course_students', [
            'course_id'    => $course->id,
            'student_code' => $user->code
        ]);
    }

    public function testShouldLeaveCourse()
    {
        $user = factory(User::class)->create();
        $course = factory(Course::class)->create();
        $course->students()->attach($user->code);

        $this->assertDatabaseHas('course_students', [
            'course_id'    => $course->id,
            'student_code' => $user->code
        ]);

        $this->signInWith($user)
            ->delete(url('v1/s/course/join/' . $course->id))
            ->assertOk()
            ->assertJson([
                'success' => true
            ]);

        $this->assertDatabaseMissing('course_students', [
            'course_id'    => $course->id,
            'student_code' => $user->code
        ]);
    }
}

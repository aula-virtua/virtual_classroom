<?php

namespace Tests\Feature\Session\Teacher;

use App\Models\Course;
use App\Models\Forum;
use Tests\Support\TeacherAuthenticate;

class ForumTest extends TeacherAuthenticate
{
    public function testShouldCreateForum()
    {
        $course = factory(Course::class)->create();

        $data = [
            'title'       => $this->faker->title,
            'description' => $this->faker->sentence(20)
        ];

        $this->withToken()
            ->post(url('v1/t/course/' . $course->id . '/forum'), $data)
            ->assertCreated()
            ->assertJsonStructure([
                'id',
                'title',
                'description',
                'createdAt',
                'updatedAt',
                'deletedAt'
            ])
            ->assertJson([
                'title'       => $data['title'],
                'description' => $data['description']
            ]);

        $this->assertDatabaseHas('forums', [
            'title'       => $data['title'],
            'description' => $data['description']
        ]);
    }

    public function testShouldUpdateForum()
    {
        $course = factory(Course::class)->create();
        $forum  = factory(Forum::class)->create([
            'course_id' => $course->id
        ]);

        $data = [
            'title' => $this->faker->title,
            'description' => $this->faker->sentence(20)
        ];

        $this->withToken()
            ->put(url('v1/t/course/' . $course->id . '/forum/' . $forum->id), $data)
            ->assertOk()
            ->assertJson([
                'title'       => $data['title'],
                'description' => $data['description']
            ])
            ->assertJsonStructure([
                'id',
                'title',
                'description',
                'createdAt',
                'updatedAt',
                'deletedAt'
            ]);

        $this->assertDatabaseHas('forums', [
            'id'          => $forum->id,
            'title'       => $data['title'],
            'description' => $data['description'],
        ]);
    }

    public function testShouldDeactivateForum()
    {
        $course = factory(Course::class)->create();
        $forum  = factory(Forum::class)->create([
            'course_id' => $course->id
        ]);

        $this->withToken()
            ->delete(url('v1/t/course/' . $course->id . '/forum/' . $forum->id))
            ->assertOk()
            ->assertJson([
                'success' => true
            ]);

        $this->assertDatabaseMissing('forums', [
            'id'         => $forum->id,
            'deleted_at' => null
        ]);
    }

    public function testShouldActivateForumAgain()
    {
        $course = factory(Course::class)->create();
        $forum  = factory(Forum::class)->create([
            'course_id' => $course->id
        ]);
        $forum->delete();

        $this->assertDatabaseMissing('forums', [
            'id'         => $forum->id,
            'deleted_at' => null
        ]);

        $this->withToken()
            ->put(url('v1/t/course/' . $course->id . '/forum/' . $forum->id), [
                'activate' => true
            ])
            ->assertJson([
                'deletedAt' => null
            ]);

        $this->assertDatabaseHas('forums', [
            'id'         => $forum->id,
            'deleted_at' => null
        ]);
    }
}

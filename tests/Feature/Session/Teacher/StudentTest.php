<?php

namespace Tests\Feature\Session\Teacher;

use App\Models\Course;
use App\Models\Homework;
use App\Models\HomeworkFile;
use App\Models\HomeworkUpload;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use MainUsersSeeder;
use Tests\Support\TeacherAuthenticate;

class StudentTest extends TeacherAuthenticate
{
    public function testShouldRetrieveFilesFromStudent()
    {
        Storage::fake('homework_files');
        $code = MainUsersSeeder::$users['student']['code'];
        $course = factory(Course::class)->create();
        $course->students()->attach($code);
        $homework = factory(Homework::class)->create([
            'course_id' => $course->id
        ]);
        $files = factory(HomeworkFile::class, 3)->create([
            'homework_id' => $homework->id,
            'user_code'   => $code
        ])->each(function ($homeworkFile) use ($code, $course, $homework) {
            $file = UploadedFile::fake()->image('file.png');
            $file->storeAs(sprintf('%s/%s/%s', $course->id, $homework->id, $code), $homeworkFile->file_name, 'homework_files');
        });

        foreach ($files as $file) {
            Storage::disk('homework_files')->assertExists(
                sprintf('%s/%s/%s', $course->id, $homework->id, $code),
                $file->file_name
            );

            $this->assertDatabaseHas('homework_files', [
                'homework_id' => $homework->id,
                'user_code' => $code,
                'file_name' => $file->file_name,
                'deleted_at' => null
            ]);
        }

        HomeworkUpload::create([
            'homework_id' => $homework->id,
            'user_code'   => $code
        ]);

        $this->assertDatabaseHas('homework_uploads', [
            'homework_id' => $homework->id,
            'user_code'   => $code
        ]);

        $this->withToken()
            ->get(url('v1/t/course/' . $course->id . '/homework/' . $homework->id . '/student/' . $code))
            ->assertOk()
            ->assertJsonStructure([
                'calification',
                'files' => [
                    [
                        'fileName',
                        'mimeType',
                        'originalName'
                    ]
                ]
            ]);
    }

    public function testShouldUpdateStudentCalification()
    {
        $code = MainUsersSeeder::$users['student']['code'];
        $course = factory(Course::class)->create();
        $course->students()->attach($code);
        $homework = factory(Homework::class)->create([
            'course_id' => $course->id
        ]);

        $this->withToken()
            ->put(url('v1/t/course/' . $course->id . '/homework/' . $homework->id . '/student/' . $code), [
                'calification' => 1
            ])
            ->assertOk()
            ->assertJson([
                'success' => true
            ]);
    }
}

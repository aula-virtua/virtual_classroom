<?php

namespace Tests\Feature\Session\Teacher;

use App\Http\Controllers\Shared\Constants\UserType;
use App\Models\Course;
use App\Models\User;
use Tests\Support\TeacherAuthenticate;

class CourseTest extends TeacherAuthenticate
{
    public function testShouldCreateCourse()
    {
        $data = [
            'name'          => $this->faker->name,
            'description'   => $this->faker->sentence(15),
            'code'          => $this->faker->randomNumber(9),
        ];

        $this->withToken()
            ->post(url('v1/t/course'), $data)
            ->assertCreated()
            ->assertJsonStructure([
                'id',
                'name',
                'description',
                'code',
                'banner',
                'createdAt',
                'updatedAt',
                'deletedAt'
            ]);

        $this->assertDatabaseHas('courses', [
            'code' => $data['code'],
            'name' => $data['name']
        ]);
    }

    public function testShouldRetrieveCourse()
    {
        $course = factory(Course::class)->create();

        $this->withToken()
            ->get(url('v1/t/course/' . $course->id))
            ->assertOk()
            ->assertJsonStructure([
                'id',
                'name',
                'description',
                'code',
                'banner',
                'createdAt',
                'updatedAt',
                'deletedAt'
            ]);
    }

    public function testShouldRetrieveCourses()
    {
        factory(Course::class)->create();

        $this->withToken()
            ->get(url('v1/t/course'))
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    [
                        'id',
                        'name',
                        'description',
                        'code',
                        'banner',
                        'createdAt',
                        'updatedAt',
                        'deletedAt'
                    ]
                ],
                'meta',
                'links' => [
                    'first',
                    'prev',
                    'last',
                    'next'
                ]
            ]);
    }

    public function testShouldUpdateCourse()
    {
        $course = factory(Course::class)->create();

        $data = [
            'name'          => $this->faker->name,
            'description'   => $this->faker->sentence(20),
            'code'          => $this->faker->randomNumber(9),
        ];

        $this->withToken()
            ->put(url("v1/t/course/" . $course->id), $data)
            ->assertOk()
            ->assertJson([
                'code'        => $data['code'],
                'description' => $data['description'],
                'name'        => $data['name']
            ]);

        $this->assertDatabaseHas('courses', [
            'id'          => $course->id,
            'code'        => $data['code'],
            'description' => $data['description'],
            'name'        => $data['name']
        ]);
    }

    public function testShouldDeactivateCourse()
    {
        $course = factory(Course::class)->create();

        $this->withToken()
            ->delete(url("v1/t/course/" . $course->id))
            ->assertOk()
            ->assertJson([
                'success' => true
            ]);

        $this->assertDatabaseMissing('courses', [
            'id'         => $course->id,
            'deleted_at' => null
        ]);
    }

    public function testValidateUnexistingCourse()
    {
        $this->withToken()
            ->get(url('v1/t/course/somefakeid'))
            ->assertNotFound()
            ->assertJson([
                'message' => 'El curso no fue encontrado.'
            ]);
    }

    public function testValidateCourseOwnership()
    {
        $teacher = factory(User::class)->create([
            'user_type' => UserType::TEACHER
        ]);
        $course = factory(Course::class)->create([
            'teacher_code' => $teacher->code
        ]);

        $this->withToken()
            ->get(url('v1/t/course/' . $course->id))
            ->assertUnauthorized()
            ->assertJson([
                'message' => 'No tienes suficientes permisos para acceder al recurso.'
            ]);
    }

    public function testShouldRetrieveStudentsFromCourse()
    {
        $course = factory(Course::class)->create();
        $students = factory(User::class, 5)->create();
        $codes = $students->map(function ($s) {
            return $s->code;
        })->toArray();
        $course->students()->attach($codes);

        $this->withToken()
            ->get(url('v1/t/course/' . $course->id . '/student'))
            ->assertOk()
            ->assertJsonStructure([
                [
                    'code',
                    'email',
                    'name',
                    'firstLastName',
                    'secondLastName',
                    'profileImage',
                    'calification',
                    'joinedAt',
                ]
            ]);
    }
}

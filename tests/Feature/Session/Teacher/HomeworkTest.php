<?php

namespace Tests\Feature\Session\Teacher;

use App\Models\Course;
use App\Models\Homework;
use App\Models\HomeworkFile;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use MainUsersSeeder;
use Tests\Support\TeacherAuthenticate;

class HomeworkTest extends TeacherAuthenticate
{
    public function testShouldRetrieveHomeworks()
    {
        $course = factory(Course::class)->create();
        $homeworks = factory(Homework::class, 5)->create([
            'course_id' => $course->id
        ]);
        $course->homeworks($homeworks);

        $this->withToken()
            ->get(url('v1/t/course/' . $course->id . '/homework'))
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    [
                        'id',
                        'name',
                        'description',
                        'deliveryDate',
                        'createdAt',
                        'updatedAt',
                        'deletedAt'
                    ]
                ],
                'meta',
                'links' => [
                    'first',
                    'prev',
                    'next',
                    'last'
                ]
            ]);
    }

    public function testShouldCreateHomework()
    {
        Storage::fake('homework_files');
        $course = factory(Course::class)->create();

        $data = [
            'name' => $this->faker->title,
            'description' => $this->faker->sentence(15),
            'delivery_date' => Carbon::now()->addDay(),
            'max_calification' => $this->faker->numberBetween(1, 5),
            'files' => [
                UploadedFile::fake()->image('image_1.png'),
                UploadedFile::fake()->image('image_2.png'),
                UploadedFile::fake()->image('image_3.png')
            ]
        ];

        $response = $this->withToken()
            ->post(url('v1/t/course/' . $course->id . '/homework'), $data);

        $response->assertCreated()
            ->assertJsonStructure([
                'id',
                'name',
                'description',
                'maxCalification',
                'deliveryDate',
                'files' => [
                    [
                        'fileName',
                        'originalName',
                        'mimeType'
                    ]
                ],
                'createdAt',
                'updatedAt',
                'deletedAt'
            ]);

        $body = $response->getOriginalContent();
        $files = $body['files'];
        $code = MainUsersSeeder::$users['teacher']['code'];

        foreach ($files as $file) {
            Storage::disk('homework_files')->assertExists(
                sprintf('%s/%s/%s', $course->id, $body['id'], $code),
                $file['fileName']
            );
        }
    }

    public function testShouldRetrieveHomeworkWithFiles()
    {
        Storage::fake('homework_files');
        $code = MainUsersSeeder::$users['teacher']['code'];
        $course = factory(Course::class)->create();
        $homework = factory(Homework::class)->create([
            'course_id' => $course->id
        ]);

        factory(HomeworkFile::class, 3)->create([
            'homework_id' => $homework->id,
            'user_code'   => $code
        ])->each(function ($homeworkFile) use ($code, $course, $homework) {
            $file = UploadedFile::fake()->image('file.png');
            $file->storeAs(sprintf('%s/%s/%s', $course->id, $homework->id, $code), $homeworkFile->file_name, 'homework_files');
        });

        $response = $this->withToken()
            ->get(url('v1/t/course/' . $course->id . '/homework/' . $homework->id));

        $response
            ->assertOk()
            ->assertJsonStructure([
                'id',
                'name',
                'description',
                'maxCalification',
                'deliveryDate',
                'files' => [
                    [
                        'fileName',
                        'originalName',
                        'mimeType'
                    ]
                ],
                'createdAt',
                'updatedAt',
                'deletedAt'
            ]);

        $body = $response->getOriginalContent();
        $files = $body['files'];

        foreach ($files as $file) {
            Storage::disk('homework_files')->assertExists(
                sprintf('%s/%s/%s', $course->id, $homework->id, $code),
                $file['fileName']
            );
        }
    }

    public function testShouldDeactivateHomework()
    {
        $course = factory(Course::class)->create();
        $homework = factory(Homework::class)->create([
            'course_id' => $course->id
        ]);

        $this->assertDatabaseHas('homeworks', [
            'id'         => $homework->id,
            'deleted_at' => null
        ]);

        $this->withToken()
            ->delete(url('v1/t/course/' . $course->id . '/homework/' . $homework->id))
            ->assertOk()
            ->assertJson([
                'success' => true
            ]);

        $this->assertDatabaseMissing('homeworks', [
            'id'         => $homework->id,
            'deleted_at' => null
        ]);
    }

    public function testShouldActivateHomeworkAgain()
    {
        $course = factory(Course::class)->create();
        $homework = factory(Homework::class)->create([
            'course_id' => $course->id
        ]);

        $homework->delete();

        $this->assertDatabaseMissing('homeworks', [
            'id'         => $homework->id,
            'deleted_at' => null
        ]);

        $this->withToken()
            ->post(url('v1/t/course/' . $course->id . '/homework/' . $homework->id), [
                'activate' => true
            ])
            ->assertOk();

        $this->assertDatabaseHas('homeworks', [
            'id'         => $homework->id,
            'deleted_at' => null
        ]);
    }

    public function testShouldUpdateHomework()
    {
        Storage::fake('homework_files');
        $code = MainUsersSeeder::$users['teacher']['code'];
        $course = factory(Course::class)->create();
        $homework = factory(Homework::class)->create([
            'course_id' => $course->id
        ]);

        $files = factory(HomeworkFile::class, 3)->create([
            'homework_id' => $homework->id,
            'user_code'   => $code
        ])->each(function ($homeworkFile) use ($code, $course, $homework) {
            $file = UploadedFile::fake()->image('file.png');
            $file->storeAs(sprintf('%s/%s/%s', $course->id, $homework->id, $code), $homeworkFile->file_name, 'homework_files');
        });

        foreach ($files as $file) {
            Storage::disk('homework_files')->assertExists(
                sprintf('%s/%s/%s', $course->id, $homework->id, $code),
                $file->file_name
            );

            $this->assertDatabaseHas('homework_files', [
                'homework_id' => $homework->id,
                'user_code' => $code,
                'file_name' => $file->file_name,
                'deleted_at' => null
            ]);
        }

        $remove = $files->map(function ($file) {
            return $file->file_name;
        })->toArray();

        $data = [
            'name'             => $this->faker->title,
            'description'      => $this->faker->sentence(20),
            'delivery_date'    => Carbon::now()->addDays(2),
            'max_calification' => $this->faker->numberBetween(1, 10),
            'files'            => [
                UploadedFile:: fake()->image('some_image.png'),
                UploadedFile:: fake()->image('other_image.png'),
            ],
            'remove' => $remove
        ];

        $response = $this->withToken()
            ->post(url('v1/t/course/' .$course->id . '/homework/' . $homework->id), $data);

        $response->assertOk()
            ->assertJson([
                'name' => $data['name'],
                'description' => $data['description'],
                'maxCalification' => $data['max_calification']
            ])
            ->assertJsonStructure([
                'createdAt',
                'updatedAt',
                'deletedAt',
                'files' => [
                    [
                        'fileName',
                        'originalName',
                        'mimeType'
                    ]
                ]
            ]);

        foreach ($files as $file) {
            $this->assertDatabaseMissing('homework_files', [
                'homework_id' => $homework->id,
                'user_code' => $code,
                'file_name' => $file->file_name,
                'deleted_at' => null
            ]);
        }

        $body = $response->getOriginalContent();
        $files = $body['files'];

        foreach ($files as $file) {
            Storage::disk('homework_files')->assertExists(
                sprintf('%s/%s/%s', $course->id, $homework->id, $code),
                $file['fileName']
            );
        }
    }
}

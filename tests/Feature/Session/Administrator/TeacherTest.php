<?php

namespace Tests\Feature\Session\Administrator;

use App\Http\Controllers\Shared\Constants\UserType;
use App\Models\Course;
use App\Models\User;
use MainUsersSeeder;
use Tests\Support\AdministratorAuthenticate;

/**
 * @author Eduardo Fuentes <eduardo.fuentes.rangel@gmail.com>
 */
class TeacherTest extends AdministratorAuthenticate
{
    public function testShouldRetrieveTeachers()
    {
        $this->withToken()
            ->json('GET', url('v1/a/teacher'))
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    [
                        'code',
                        'name',
                        'firstLastName',
                        'secondLastName',
                        'email',
                        'profileImage',
                        'activationStatus',
                        'createdAt',
                        'updatedAt',
                        'deletedAt'
                    ]
                ],
                'meta',
                'links' => [
                    'first',
                    'prev',
                    'last',
                    'next'
                ]
            ]);
    }

    public function testShouldCreateTeacher()
    {
        $data = [
            'code' => $this->faker->randomNumber(9),
            'name' => $this->faker->name,
            'first_last_name' => $this->faker->lastName,
            'second_last_name' => $this->faker->lastName,
            'email' => $this->faker->email
        ];

        $this->withToken()
            ->json('POST', url('v1/a/teacher'), $data)
            ->assertStatus(201)
            ->assertJsonStructure([
                'code',
                'name',
                'firstLastName',
                'secondLastName',
                'email',
                'profileImage',
                'activationStatus',
                'createdAt',
                'updatedAt',
                'deletedAt'
            ])->assertJson([
                'code' => $data['code'],
                'name' => $data['name'],
                'firstLastName' => $data['first_last_name'],
                'secondLastName' => $data['second_last_name'],
                'email' => $data['email']
            ]);

        $this->assertDatabaseHas('users', [
            'code' => $data['code']
        ]);
    }

    public function testShouldRetrieveTeacher()
    {
        $code = MainUsersSeeder::$users['teacher']['code'];
        $course = factory(Course::class)->create();

        $this->assertDatabaseHas('courses', [
            'teacher_code' => $code,
            'id'           => $course->id
        ]);

        $this->withToken()
            ->get(url('v1/a/teacher/' . $code))
            ->assertOk()
            ->assertJsonStructure([
                'code',
                'name',
                'firstLastName',
                'secondLastName',
                'email',
                'profileImage',
                'activationStatus',
                'createdAt',
                'updatedAt',
                'deletedAt',
                'courses' => [
                    [
                        'id',
                        'name',
                        'description',
                        'code',
                        'banner',
                        'createdAt',
                        'updatedAt',
                        'deletedAt'
                    ]
                ]
            ]);
    }

    public function testShouldUpdateTeacherEmail()
    {
        $code = factory(User::class)->create([
            'user_type' => UserType::TEACHER
        ])->code;

        $email = $this->faker->email;

        $this->withToken()
            ->put(url("v1/a/teacher/${code}"), [
                'email' => $email
            ])
            ->assertStatus(200)
            ->assertJson([
                'email' => $email
            ]);

        $this->assertDatabaseHas('users', [
            'code' => $code,
            'email' => $email
        ]);
    }

    public function testShouldActivateTeacherAgain()
    {
        $teacher = factory(User::class)->create([
            'user_type' => UserType::TEACHER
        ]);
        $code = $teacher->code;
        $teacher->delete();

        $this->withToken()
            ->put(url("v1/a/teacher/${code}"), [
                'activate' => true
            ])
            ->assertStatus(200)
            ->assertJson([
                'deletedAt' => null
            ]);

        $this->assertDatabaseHas('users', [
            'code' => $code,
            'deleted_at' => null
        ]);
    }

    public function testShouldDeactivateTeacher()
    {
        $code = factory(User::class)->create([
            'user_type' => UserType::TEACHER
        ])->code;

        $this->withToken()
            ->delete(url("v1/a/teacher/${code}"))
            ->assertOk()
            ->assertJson([
                'success' => true
            ]);

        $this->assertDatabaseMissing('users', [
            'code'       => $code,
            'deleted_at' => null
        ]);
    }

    public function testShouldFailedWithUnexistingTeacher()
    {
        $this->withToken()
            ->delete(url('v1/a/teacher/fakeid'))
            ->assertNotFound()
            ->assertJson([
                'message' => 'El maestro no fue encontrado.'
            ]);
    }
}

<?php

namespace Tests\Feature\Session\Administrator;

use App\Http\Controllers\Shared\Constants\UserType;
use App\Models\User;
use Tests\Support\AdministratorAuthenticate;

class StudentTest extends AdministratorAuthenticate
{
    public function testShouldRetrieveStudents()
    {
        factory(User::class, 5)->create([
            'user_type' => UserType::STUDENT
        ]);

        $this->withToken()
            ->get(url('v1/a/student'))
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    [
                        'code',
                        'name',
                        'firstLastName',
                        'secondLastName',
                        'email',
                        'profileImage',
                        'activationStatus',
                        'createdAt',
                        'updatedAt',
                        'deletedAt'
                    ]
                ],
                'meta',
                'links' => [
                    'first',
                    'prev',
                    'last',
                    'next'
                ]
            ]);
    }

    public function testShouldUpdateStudentEmail()
    {
        $student = factory(User::class)->create([
            'user_type' => UserType::STUDENT
        ]);

        $email = $this->faker->email;

        $this->withToken()
            ->put(url('v1/a/student/' . $student->code), [
                'email' => $email
            ])->assertOk()
            ->assertJson([
                'email' => $email
            ]);

        $this->assertDatabaseHas('users', [
            'email' => $email
        ]);
    }

    public function testShouldDeactivateStudent()
    {
        $student = factory(User::class)->create([
            'user_type' => UserType::STUDENT
        ]);

        $this->withToken()
            ->delete(url('v1/a/student/' . $student->code))
            ->assertOk()
            ->assertJson([
                'success' => true
            ]);

        $this->assertDatabaseMissing('users', [
            'code'       => $student->code,
            'deleted_at' => null
        ]);
    }
}

<?php

namespace Tests\Feature\Session\Shared;

use App\Models\Comment;
use App\Models\Course;
use App\Models\Forum;
use App\Models\User;
use Tests\TestCase;

class CommentTest extends TestCase
{
    public function testShouldRetrieveComments()
    {
        $user   = factory(User::class)->create();
        $course = factory(Course::class)->create();
        $course->students()->attach($user->code);
        $forum = factory(Forum::class)->create([
            'course_id' => $course->id
        ]);
        factory(Comment::class, 5)->create([
            'forum_id'  => $forum->id,
            'user_code' => $user->code
        ]);

        $this->signInWith($user)
            ->get(url('v1/sh/course/' . $course->id . '/forum/' . $forum->id . '/comment'))
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    [
                        'title',
                        'description',
                        'createdBy' => [
                            'code',
                            'name',
                            'firstLastName',
                            'secondLastName',
                            'profileImage'
                        ],
                        'createdAt',
                        'updatedAt',
                        'deletedAt',
                    ]
                ],
                'meta',
                'links' => [
                    'first',
                    'last',
                    'prev',
                    'next'
                ]
            ]);
    }

    public function testShouldRetrieveNestedComments()
    {
        $user   = factory(User::class)->create();
        $course = factory(Course::class)->create();
        $course->students()->attach($user->code);
        $forum = factory(Forum::class)->create([
            'course_id' => $course->id
        ]);
        $parent = factory(Comment::class)->create([
            'forum_id'  => $forum->id,
            'user_code' => $user->code
        ]);
        factory(Comment::class, 5)->create([
            'forum_id' => $forum->id,
            'user_code' => $user->code,
            'comment_id' => $parent->id
        ]);

        $this->signInWith($user)
            ->get(url('v1/sh/course/' . $course->id . '/forum/' . $forum->id . '/comment/' . $parent->id))
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    [
                        'title',
                        'description',
                        'createdBy' => [
                            'code',
                            'name',
                            'firstLastName',
                            'secondLastName',
                            'profileImage'
                        ],
                        'createdAt',
                        'updatedAt',
                        'deletedAt',
                    ]
                ],
                'meta',
                'links' => [
                    'first',
                    'last',
                    'prev',
                    'next'
                ]
            ]);
    }

    public function testShouldCreateInitialCommentOrResponse()
    {
        $user = factory(User::class)->create();
        $course = factory(Course::class)->create();
        $course->students()->attach($user->code);
        $forum = factory(Forum::class)->create([
            'course_id' => $course->id
        ]);

        $data = [
            'title'       => $this->faker->title,
            'description' => $this->faker->sentence(20)
        ];

        $this->signInWith($user)
            ->post(url('v1/sh/course/' . $course->id . '/forum/' . $forum->id . '/comment'), $data)
            ->assertCreated()
            ->assertJson([
                'title'       => $data['title'],
                'description' => $data['description'],
                'createdBy'   => [
                    'name'           => $user->name,
                    'code'           => $user->code,
                    'firstLastName'  => $user->first_last_name,
                    'secondLastName' => $user->second_last_name,
                    'profileImage'   => $user->profile_image
                ]
            ])
            ->assertJsonStructure([
                'id',
                'createdAt',
                'updatedAt',
                'deletedAt'
            ]);

        $data = [
            'title'       => $this->faker->title,
            'description' => $this->faker->sentence(20)
        ];

        $comment = factory(Comment::class)->create([
            'user_code' => $user->code,
            'forum_id' => $forum->id
        ]);

        $this->signInWith($user)
            ->post(url('v1/sh/course/' . $course->id . '/forum/' . $forum->id . '/comment/' . $comment->id), $data)
            ->assertCreated()
            ->assertJson([
                'title'       => $data['title'],
                'description' => $data['description'],
                'createdBy'   => [
                    'name'           => $user->name,
                    'code'           => $user->code,
                    'firstLastName'  => $user->first_last_name,
                    'secondLastName' => $user->second_last_name,
                    'profileImage'   => $user->profile_image
                ]
            ])
            ->assertJsonStructure([
                'id',
                'createdAt',
                'updatedAt',
                'deletedAt'
            ]);
    }

    public function testShouldUpdateComment()
    {
        $user = factory(User::class)->create();
        $course = factory(Course::class)->create();
        $course->students()->attach($user->code);
        $forum = factory(Forum::class)->create([
            'course_id' => $course->id
        ]);
        $comment = factory(Comment::class)->create([
            'user_code' => $user->code,
            'forum_id' => $forum->id
        ]);

        $data = [
            'title'       => $this->faker->title,
            'description' => $this->faker->sentence(20)
        ];

        $this->signInWith($user)
            ->put(url('v1/sh/course/' . $course->id . '/forum/' . $forum->id . '/comment/' . $comment->id), $data)
            ->assertOk()
            ->assertJson([
                'title' => $data['title'],
                'description' => $data['description']
            ]);
    }

    public function testShouldDeleteComment()
    {
        $user = factory(User::class)->create();
        $course = factory(Course::class)->create();
        $course->students()->attach($user->code);
        $forum = factory(Forum::class)->create([
            'course_id' => $course->id
        ]);
        $comment = factory(Comment::class)->create([
            'user_code' => $user->code,
            'forum_id' => $forum->id
        ]);

        $this->signInWith($user)
            ->delete(url('v1/sh/course/' . $course->id . '/forum/' . $forum->id . '/comment/' . $comment->id))
            ->assertOk()
            ->assertJson([
                'success' => true
            ]);
    }
}

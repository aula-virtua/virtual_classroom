<?php

namespace Tests\Feature\Session\Shared;

use App\Http\Controllers\Shared\Constants\UserType;
use App\Models\Course;
use App\Models\User;
use Tests\TestCase;

class CourseMiddlewareTest extends TestCase
{
    public function testSharedAccess()
    {
        $student = factory(User::class)->create(['user_type' => UserType::STUDENT]);
        $teacher = factory(User::class)->create(['user_type' => UserType::TEACHER]);
        $administrator = factory(User::class)->create(['user_type' => UserType::ADMINISTRATOR]);

        $course = factory(Course::class)->create([
            'teacher_code' => $teacher->code
        ]);
        $course->students()->attach($student->code);

        $this->signInWith($student)
            ->get(url('v1/sh/course/' . $course->id . '/forum'))
            ->assertOk();

        $this->signInWith($teacher)
            ->get(url('v1/sh/course/' . $course->id . '/forum'))
            ->assertOk();

        $this->signInWith($administrator)
            ->get(url('v1/sh/course/' . $course->id . '/forum'))
            ->assertOk();
    }

    public function testDeniedAccess()
    {
        $teacher = factory(User::class)->create(['user_type' => UserType::TEACHER]);
        $student = factory(User::class)->create(['user_type' => UserType::STUDENT]);
        $course = factory(Course::class)->create();

        $this->signInWith($teacher)
            ->get(url('v1/sh/course/' . $course->id . '/forum'))
            ->assertUnauthorized();

        $this->signInWith($student)
            ->get(url('v1/sh/course/' . $course->id . '/forum'))
            ->assertUnauthorized();
    }
}

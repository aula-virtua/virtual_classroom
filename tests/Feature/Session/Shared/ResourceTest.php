<?php

namespace Tests\Feature\Session\Shared;

use App\Models\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ResourceTest extends TestCase
{
    use WithoutMiddleware;

    public function testRetrieveProfileImage()
    {
        $user = factory(User::class)->create();

        $data = [
            'name'             => $this->faker->name,
            'first_last_name'  => $this->faker->lastName,
            'second_last_name' => $this->faker->lastName,
            'email'            => $this->faker->email,
            'profile_image'    => UploadedFile::fake()->image('avatar.png')
        ];

        $response = $this->signInWith($user)
            ->post(url('v1/sh/auth'), $data);

        $response->assertOk()
            ->assertJsonStructure([
                'profileImage'
            ]);

        $body = $response->getOriginalContent();

        Storage::disk('public')->assertExists(sprintf('%s/%s', $user->code, $body['profile_image']));
        $token = $this->token;

        $this->get(url('v1/sh/resource?type=profile&who=' . $user->code . '&fileName=' . $body['profile_image'] . '&token=' . $token))
            ->assertOk();
    }
}

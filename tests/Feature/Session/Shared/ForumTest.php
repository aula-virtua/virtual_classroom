<?php

namespace Tests\Feature\Session\Shared;

use App\Models\Course;
use App\Models\Forum;
use App\Models\User;
use Tests\TestCase;

class ForumTest extends TestCase
{
    public function testShouldRetrieveForums()
    {
        $user   = factory(User::class)->create();
        $course = factory(Course::class)->create();
        $course->students()->attach($user->code);
        factory(Forum::class, 3)->create([
            'course_id' => $course->id
        ]);

        $this->signInWith($user)
            ->get(url('v1/sh/course/' . $course->id . '/forum'))
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    [
                        'id',
                        'title',
                        'description',
                        'createdAt',
                        'updatedAt',
                        'deletedAt'
                    ]
                ],
                'meta',
                'links' => [
                    'first',
                    'last',
                    'prev',
                    'next'
                ]
            ]);
    }
}

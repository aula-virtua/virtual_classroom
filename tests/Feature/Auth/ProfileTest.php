<?php

namespace Tests\Feature\Auth;

use App\Http\Controllers\Shared\Constants\UserType;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use MainUsersSeeder;
use Tests\TestCase;

class ProfileTest extends TestCase
{
    public function testRetrieveProfile()
    {
        $this->signInAs(UserType::ADMINISTRATOR)
            ->json('GET', url('v1/sh/auth'))
            ->assertOk()
            ->assertJsonStructure([
                'code',
                'name',
                'firstLastName',
                'secondLastName',
                'email',
                'profileImage',
                'activationStatus',
                'createdAt',
                'updatedAt',
                'deletedAt'
            ])
            ->assertJson([
                'code' => 218746956,
                'name' => 'Eduardo',
                'firstLastName' => 'Fuentes',
                'secondLastName' => 'Rangel',
                'email' => 'eduardo.fuentes.rangel@gmail.com'
            ]);
    }

    public function testLogOutUser()
    {
        $this->signInAs(UserType::STUDENT);
        $token = $this->token;

        $this->json('GET', url('v1/sh/auth/logout'))
            ->assertOk()
            ->assertJson([
                'success' => true
            ]);

        $this->json('GET', url('v1/sh/auth'), [], [
            'Authorization' => "Bearer ${token}"
        ])
            ->assertUnauthorized()
            ->assertJson([
                'message' => 'El token proveído ya no es válido.'
            ]);
    }

    public function testChangePasswordValidations()
    {
        $this->signInAs(UserType::STUDENT)
            ->json('POST', url('v1/sh/auth/password'), [
                'current_password'      => MainUsersSeeder::$users['student']['password'],
                'password'              => MainUsersSeeder::$users['student']['password'],
                'password_confirmation' => MainUsersSeeder::$users['student']['password'],
            ])
            ->assertStatus(422)
            ->assertJson([
                'errors' => [
                    'password' => ['Los campos contraseña y contraseña actual deben ser diferentes.']
                ]
            ]);

        $this->signInAs(UserType::STUDENT)
            ->json('POST', url('v1/sh/auth/password'), [
                'current_password'      => 'someinvalidpassword',
                'password'              => MainUsersSeeder::$users['student']['password'],
                'password_confirmation' => MainUsersSeeder::$users['student']['password'],
            ])
            ->assertStatus(422)
            ->assertJson([
                'errors' => [
                    'current_password' => ['La contraseña actual no es correcta.']
                ]
            ]);

        $this->signInAs(UserType::STUDENT)
            ->json('POST', url('v1/sh/auth/password'), [
                'current_password'      => MainUsersSeeder::$users['student']['password'],
                'password'              => '123456789',
                'password_confirmation' => '123456789',
            ])
            ->assertStatus(422)
            ->assertJson([
                'errors' => [
                    'password' => ['El campo contraseña debe contener al menos 10 caracteres.']
                ]
            ]);

        $this->signInAs(UserType::STUDENT)
            ->json('POST', url('v1/sh/auth/password'), [
                'current_password'      => MainUsersSeeder::$users['student']['password'],
                'password'              => '1234567890',
                'password_confirmation' => '1234567891',
            ])
            ->assertStatus(422)
            ->assertJson([
                'errors' => [
                    'password' => ['El campo confirmación de contraseña no coincide.']
                ]
            ]);
    }

    public function testChangePassword()
    {
        $user = factory(User::class)->create();

        $this->signInWith($user)
            ->json('POST', url('v1/sh/auth/password'), [
                'current_password'      => '12345',
                'password'              => 'other12345',
                'password_confirmation' => 'other12345'
            ])->assertOk()
            ->assertJson([
                'success' => true
            ]);
    }

    public function testShouldUpdateProfileInformation()
    {
        $user = factory(User::class)->create();
        Storage::fake('public');

        $response = $this->signInWith($user)
            ->json('POST', url('v1/sh/auth'), [
                'name'             => 'Another',
                'first_last_name'  => 'Testing',
                'second_last_name' => 'Account',
                'profile_image'    => UploadedFile::fake()->image('avatar.png'),
                'email'            => 'testing.account@gmail.com'
            ]);

        $response->assertOk()
            ->assertJson([
                'code' => $user->code,
                'name' => 'Another',
                'firstLastName' => 'Testing',
                'secondLastName' => 'Account',
            ])
            ->assertJsonStructure([
                'profileImage',
                'createdAt',
                'updatedAt',
                'deletedAt'
            ]);

        $body = $response->getOriginalContent();

        Storage::disk('public')->assertExists(sprintf('%s/%s', $user->code, $body['profileImage']));
    }
}

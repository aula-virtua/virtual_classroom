<?php

namespace Tests\Feature\Auth;

use App\Http\Controllers\Shared\Constants\UserType;
use App\Models\User;
use MainUsersSeeder;
use Tests\TestCase;

class ForgotPasswordTest extends TestCase
{
    public function testAskForChangeOfPasswordWithEmail()
    {
        $email = MainUsersSeeder::$users['student']['email'];

        $this->json('POST', url('v1/auth/forgot'), [
            'code' => $email
        ])->assertOk();

        $this->assertDatabaseMissing('users', [
            'email'                  => $email,
            'reset_token_created_at' => null
        ]);
    }

    public function testAskForChangeOfPasswordWithCode()
    {
        $code = MainUsersSeeder::$users['teacher']['code'];

        $this->json('POST', url('v1/auth/forgot'), [
            'code' => $code
        ])->assertOk();

        $this->assertDatabaseMissing('users', [
            'code'                   => $code,
            'reset_token_created_at' => null
        ]);
    }

    public function testChangePassword()
    {
        $code = factory(User::class)->create([
            'user_type' => UserType::STUDENT
        ])->code;

        $this->json('POST', url('v1/auth/forgot'), [
            'code' => $code
        ])->assertOk();

        $this->assertDatabaseMissing('users', [
            'code'                   => $code,
            'reset_token_created_at' => null
        ]);

        $token = User::where('code', $code)->first()->reset_token;

        $this->json('POST', url('v1/auth/reset'), [
            'password'              => 'teacher1234',
            'password_confirmation' => 'teacher1234',
            'token'                 => $token
        ])
            ->assertStatus(201)
            ->assertJson([
                'success' => true
            ]);

        $this->assertDatabaseHas('users', [
            'code' => $code,
            'reset_token' => '',
            'reset_token_created_at' => null
        ]);
    }
}

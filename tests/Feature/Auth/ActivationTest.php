<?php

namespace Tests\Feature\Auth;

use App\Http\Controllers\Shared\Constants\UserType;
use App\Models\User;
use MainUsersSeeder;
use Tests\TestCase;

class ActivationTest extends TestCase
{
    public function testActivateAccount()
    {
        $code = MainUsersSeeder::$users['student']['code'];

        $token = User::find($code)
            ->first()
            ->activation_token;

        $this->signInAs(UserType::STUDENT)
            ->json('GET', url("v1/auth/verify/${token}"))
            ->assertOk()
            ->assertJson([
                'success' => true
            ]);

        $this->assertDatabaseHas('users', [
            'code' => $code,
            'activation_status' => 1,
            'activation_token' => ''
        ]);
    }

    public function testInvalidActivationToken()
    {
        $code = MainUsersSeeder::$users['teacher']['code'];

        $this->signInAs(UserType::TEACHER)
            ->json('GET', url("v1/auth/verify/fake_token"))
            ->assertStatus(400)
            ->assertJson([
                'message' => 'El token de activación no es válido.'
            ]);

        $this->assertDatabaseHas('users', [
            'code' => $code,
            'activation_status' => 0,
        ]);
    }
}

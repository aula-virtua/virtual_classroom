<?php

namespace Tests\Feature\Auth;

use Illuminate\Support\Str;
use Tests\TestCase;

/**
 * Test for the Register Flow
 *
 * @author Eduardo Fuentes <eduardo.fuentes.rangel@gmail.com>
 */
class RegisterStudentTest extends TestCase
{
    /**
     * Should create a student
     *
     * @return void
     */
    public function testCreateStudent()
    {
        $code = $this->faker->randomNumber(9);
        $password = Str::random(10);

        $this->json('POST', url('v1/auth/register'), [
            'code'                  => $code,
            'name'                  => $this->faker->name,
            'first_last_name'       => $this->faker->lastName,
            'second_last_name'      => $this->faker->lastName,
            'password'              => $password,
            'password_confirmation' => $password,
            'email'                 => $this->faker->email
        ])->assertStatus(201)
            ->assertJson([
                'success' => true
            ]);

        $this->assertDatabaseHas('users', [
            'code' => $code
        ]);
    }

    /**
     * Should match for the validation rules.
     *
     * @return void
     */
    public function testValidationErrors()
    {
        $this->json('POST', url('v1/auth/register'))
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'code'             => ['El campo código es obligatorio.'],
                    'name'             => ['El campo nombre es obligatorio.'],
                    'first_last_name'  => ['El campo apellido paterno es obligatorio.'],
                    'second_last_name' => ['El campo apellido materno es obligatorio.'],
                    'password'         => ['El campo contraseña es obligatorio.'],
                    'email'            => ['El campo correo es obligatorio.']
                ]
            ]);
    }
}

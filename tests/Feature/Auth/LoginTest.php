<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Support\Str;
use MainUsersSeeder;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testInvalidCredentials()
    {
        $this->json('POST', url('v1/auth/login'), [
            'code' => $this->faker->randomNumber(9),
            'password' => Str::random(10)
        ])->assertStatus(401)
            ->assertJson([
                'message' => 'Credenciales Inválidas.'
            ]);
    }

    public function testSignInAdministrator()
    {
        $response = $this->json('POST', url('v1/auth/login'), [
            'code'     => MainUsersSeeder::$users['administrator']['code'],
            'password' => MainUsersSeeder::$users['administrator']['password']
        ]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'token',
                'type',
                'expires_in'
            ]);

        $this->assertToken($response->getOriginalContent()['token']);
    }

    public function testSignInTeacher()
    {
        $response = $this->json('POST', url('v1/auth/login'), [
            'code'     => MainUsersSeeder::$users['teacher']['code'],
            'password' => MainUsersSeeder::$users['teacher']['password']
        ]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'token',
                'type',
                'expires_in'
            ]);

        $this->assertToken($response->getOriginalContent()['token']);
    }

    public function testSignInStudent()
    {
        $response = $this->json('POST', url('v1/auth/login'), [
            'code'     => MainUsersSeeder::$users['student']['code'],
            'password' => MainUsersSeeder::$users['student']['password']
        ]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'token',
                'type',
                'expires_in'
            ]);

        $this->assertToken($response->getOriginalContent()['token']);
    }

    public function testSignInWithEmail()
    {
        $response = $this->json('POST', url('v1/auth/login'), [
            'code'     => MainUsersSeeder::$users['student']['email'],
            'password' => MainUsersSeeder::$users['student']['password']
        ]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'token',
                'type',
                'expires_in'
            ]);

        $this->assertToken($response->getOriginalContent()['token']);
    }

    public function testSuspendedAccount()
    {
        $code = MainUsersSeeder::$users['student']['code'];

        User::where('code', $code)->first()->delete();

        $this->assertDatabaseMissing('users', [
            'code' => $code,
            'deleted_at' => null
        ]);

        $this->json('POST', url('v1/auth/login'), [
            'code'     => $code,
            'password' => MainUsersSeeder::$users['student']['password']
        ])->assertUnauthorized()
            ->assertJson([
                'message' => 'Cuenta se encuentra suspendida.'
            ]);


        User::withTrashed()->where('code', $code)->first()->restore();
    }
}

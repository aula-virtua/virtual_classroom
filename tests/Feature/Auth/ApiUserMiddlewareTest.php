<?php

namespace Tests\Feature\Auth;

use App\Http\Controllers\Shared\Constants\UserType;
use Tests\TestCase;

/**
 * @author Eduardo Fuentes <eduardo.fuentes.rangel@gmail.com>
 */
class ApiUserMiddlewareTest extends TestCase
{
    public function testAdministratorAccess()
    {
        $this->signInAs(UserType::ADMINISTRATOR);
        $atoken = $this->token;

        $this->signInAs(UserType::TEACHER);
        $ttoken = $this->token;

        $this->signInAs(UserType::STUDENT);
        $stoken = $this->token;

        $this->json('GET', url('v1/a/teacher'), [], [
            'Authorization' => "Bearer ${atoken}"
        ])->assertOk();

        $this->json('GET', url('v1/a/teacher'), [], [
            'Authorization' => "Bearer ${ttoken}"
        ])
            ->assertUnauthorized()
            ->assertJson([
                'message' => 'Acceso Denegedo. Probablemente no tienes suficientes permisos para usar: ' . url('v1/a/teacher')
            ]);

        $this->json('GET', url('v1/a/teacher'), [], [
            'Authorization' => "Bearer ${stoken}"
        ])
            ->assertUnauthorized()
            ->assertJson([
                'message' => 'Acceso Denegedo. Probablemente no tienes suficientes permisos para usar: ' . url('v1/a/teacher')
            ]);
    }

    public function testSharedAccess()
    {
        $this->signInAs(UserType::ADMINISTRATOR);
        $atoken = $this->token;

        $this->signInAs(UserType::TEACHER);
        $ttoken = $this->token;

        $this->signInAs(UserType::STUDENT);
        $stoken = $this->token;

        $this->json('GET', url('v1/sh/auth'), [], [
            'Authorization' => "Bearer ${atoken}"
        ])->assertOk();

        $this->json('GET', url('v1/sh/auth'), [], [
            'Authorization' => "Bearer ${ttoken}"
        ])->assertOk();

        $this->json('GET', url('v1/sh/auth'), [], [
            'Authorization' => "Bearer ${stoken}"
        ])->assertOk();
    }

    public function testTeacherAccess()
    {
        $this->signInAs(UserType::ADMINISTRATOR);
        $atoken = $this->token;

        $this->signInAs(UserType::TEACHER);
        $ttoken = $this->token;

        $this->signInAs(UserType::STUDENT);
        $stoken = $this->token;

        $this->get(url('v1/t/course'), [
            'Authorization' => "Bearer ${ttoken}"
        ])->assertOk();

        $this->json('GET', url('v1/t/course'), [], [
            'Authorization' => "Bearer ${atoken}"
        ])
            ->assertUnauthorized()
            ->assertJson([
                'message' => 'Acceso Denegedo. Probablemente no tienes suficientes permisos para usar: ' . url('v1/t/course')

            ]);

        $this->json('GET', url('v1/t/course'), [], [
            'Authorization' => "Bearer ${stoken}"
        ])
            ->assertUnauthorized()
            ->assertJson([
                'message' => 'Acceso Denegedo. Probablemente no tienes suficientes permisos para usar: ' . url('v1/t/course')
            ]);
    }
}

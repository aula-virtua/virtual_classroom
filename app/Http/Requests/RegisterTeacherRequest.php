<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterTeacherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'             => 'required|numeric|digits:9|unique:users',
            'name'             => 'required|string|max:40',
            'first_last_name'  => 'required|string|max:40',
            'second_last_name' => 'required|string|max:40',
            'email'            => 'required|string|email|unique:users'
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'code'             => 'código',
            'name'             => 'nombre',
            'first_last_name'  => 'apellido paterno',
            'second_last_name' => 'apellido materno',
            'email'            => 'correo'
        ];
    }
}

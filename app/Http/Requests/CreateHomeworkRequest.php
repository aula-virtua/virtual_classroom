<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateHomeworkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'             => 'required|string|max:100',
            'description'      => 'required|string',
            'max_calification' => 'required|numeric',
            'delivery_date'    => 'required|date|after:now',
            'files'            => 'array',
            'files.*'          => 'file|mimes:jpeg,png,avi,pdf,mp4'
        ];
    }

    public function attributes()
    {
        return [
            'name'             => 'nombre',
            'description'      => 'descripción',
            'max_calification' => 'calificación máxima',
            'delivery_date'    => 'fecha de entrega',
        ];
    }
}

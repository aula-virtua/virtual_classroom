<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Request Validator for creating a new student from the front-end of our
 * application.
 *
 * @author Eduardo Fuentes <eduardo.fuentes.rangel@gmail.com>
 */
class RegisterStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'             => 'required|numeric|unique:users',
            'name'             => 'required|string|max:40',
            'first_last_name'  => 'required|string|max:40',
            'second_last_name' => 'required|string|max:40',
            'password'         => 'required|string|confirmed|min:10',
            'email'            => 'required|string|email|unique:users'
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'code'             => 'código',
            'name'             => 'nombre',
            'first_last_name'  => 'apellido paterno',
            'second_last_name' => 'apellido materno',
            'password'         => 'contraseña',
            'email'            => 'correo',
        ];
    }
}

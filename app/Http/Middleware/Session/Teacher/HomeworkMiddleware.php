<?php

namespace App\Http\Middleware\Session\Teacher;

use App\Http\Controllers\Shared\Helpers\CommonResponse;
use App\Models\Homework;
use Closure;

class HomeworkMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id  = $request->route('id');
        $hid = $request->route('hid');

        $homework = Homework::withTrashed()->find($hid);

        if (!$homework) {
            return CommonResponse::resourceNotFound('api.homework');
        }

        if ($homework->course_id !== ((int) $id)) {
            return CommonResponse::missingPermissions();
        }

        return $next($request);
    }
}

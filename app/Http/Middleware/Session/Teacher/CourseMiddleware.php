<?php

namespace App\Http\Middleware\Session\Teacher;

use App\Http\Controllers\Shared\Helpers\CommonResponse;
use App\Models\Course;
use Closure;

class CourseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = $request->route('id');

        $course = Course::find($id);

        if (!$course) {
            return CommonResponse::resourceNotFound('api.course');
        }

        $user = $request->user();

        if ($course->teacher_code !== $user->code) {
            return CommonResponse::missingPermissions();
        }

        return $next($request);
    }
}

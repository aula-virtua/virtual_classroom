<?php

namespace App\Http\Middleware\Session\Shared;

use App\Http\Controllers\Shared\Helpers\CommonResponse;
use App\Models\Forum;
use Closure;

class ForumMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id  = $request->route('id');
        $fid = $request->route('fid');

        $forum = Forum::find($fid);

        if (!$forum) {
            return CommonResponse::resourceNotFound();
        }

        if ($forum->course_id !== ((int) $id)) {
            return CommonResponse::missingPermissions();
        }

        return $next($request);
    }
}

<?php

namespace App\Http\Middleware\Session\Shared;

use App\Http\Controllers\Shared\Constants\UserType;
use App\Http\Controllers\Shared\Helpers\CommonResponse;
use App\Models\Comment;
use Closure;

class CommentMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $cid = $request->route('cid');

        $comment = Comment::find($cid);

        if (!$comment) {
            return CommonResponse::resourceNotFound('api.comment');
        }

        $user = $request->user();

        if (((int)$user->user_type) !== UserType::ADMINISTRATOR && $comment->user_code !== $user->code) {
            return CommonResponse::missingPermissions();
        }

        return $next($request);
    }
}

<?php

namespace App\Http\Middleware\Session\Shared;

use Closure;
use Illuminate\Support\Facades\Storage;

class ResourceMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $type = $request->type;

        if ($type === 'file') {
            $course   = $request->course;
            $homework = $request->homework;
            $who      = $request->who;
            $fileName = $request->fileName;

            $path = sprintf('%s\%s\%s\%s', $course, $homework, $who, $fileName);

            if (Storage::disk('homework_files')->exists($path)) {
                return $next($request);
            }
        } elseif ($type === 'profile') {
            $fileName = $request->fileName;
            $who      = $request->who;
            $path     = sprintf('%s\%s', $who, $fileName);

            if (Storage::disk('public')->exists($path)) {
                return $next($request);
            }
        }

        return response()->file(storage_path('app\public\default\profile.png'));
    }
}

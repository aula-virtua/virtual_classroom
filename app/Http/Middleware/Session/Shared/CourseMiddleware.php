<?php

namespace App\Http\Middleware\Session\Shared;

use App\Http\Controllers\Shared\Constants\UserType;
use App\Http\Controllers\Shared\Helpers\CommonResponse;
use App\Models\Course;
use App\Models\CourseStudent;
use Closure;

class CourseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = $request->route('id');
        $course = Course::find($id);

        if (!$course) {
            return CommonResponse::resourceNotFound('api.forum');
        }

        $user = $request->user();

        if ($user->user_type === UserType::ADMINISTRATOR) {
            return $next($request);
        } elseif ($user->user_type === UserType::TEACHER) {
            if ($course->teacher_code === $user->code) {
                return $next($request);
            }
        } else {
            $belongs = CourseStudent::where('course_id', $id)
                ->where('student_code', $user->code)
                ->first();

            if ($belongs) {
                return $next($request);
            }
        }

        return CommonResponse::missingPermissions();
    }
}

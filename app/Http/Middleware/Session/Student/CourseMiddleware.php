<?php

namespace App\Http\Middleware\Session\Student;

use App\Http\Controllers\Shared\Helpers\CommonResponse;
use App\Models\Course;
use App\Models\CourseStudent;
use Closure;

class CourseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = $request->route('id');

        $course = Course::find($id);

        if (!$course) {
            return CommonResponse::resourceNotFound('api.course');
        }

        $user = $request->user();
        $belongsTo = CourseStudent::where('course_id', $course->id)
            ->where('student_code', $user->code)
            ->first();

        if (!$belongsTo) {
            return CommonResponse::missingPermissions();
        }

        return $next($request);
    }
}

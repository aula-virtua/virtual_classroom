<?php

namespace App\Http\Middleware\Auth;

use App\Exceptions\Auth\InvalidUserAccess;
use App\Http\Controllers\Shared\Constants\UserType;
use Closure;
use Exception;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class ApiUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, string $allowedRoles)
    {
        $roles = explode('|', $allowedRoles);

        try {
            $token = JWTAuth::parseToken();
            $type = $token->getPayload()->get('typ');
            $type = UserType::toString((int) $type);

            if (in_array($type, $roles)) {
                return $next($request);
            }

            throw new InvalidUserAccess();
        } catch (Exception $e) {
            throw $e;
        }

        return $next($request);
    }
}

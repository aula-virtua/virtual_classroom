<?php

namespace App\Http\Controllers\Shared\Constants;

/**
 * Types of users of the application.
 *
 * @author Eduardo Fuentes <eduardo.fuentes.rangel@gmail.com>
 */
abstract class UserType
{
    const ADMINISTRATOR = 1;

    const TEACHER = 2;

    const STUDENT = 3;

    public static function toString(int $type)
    {
        $arr = [
            static::ADMINISTRATOR => 'administrator',
            static::TEACHER       => 'teacher',
            static::STUDENT       => 'student'
        ];

        if (array_key_exists($type, $arr)) {
            return $arr[$type];
        } else {
            return null;
        }
    }
}

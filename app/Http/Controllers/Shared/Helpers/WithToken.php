<?php

namespace App\Http\Controllers\Shared\Helpers;

use App\Models\Token;
use App\Models\User;
use Illuminate\Http\Request;

/**
 * Helper trait for standarized responses with token related information.
 *
 * @author Eduardo Fuentes <eduardo.fuentes.rangel@gmail.com>
 */
trait WithToken
{
    public function responseWithToken(string $token)
    {
        return response()->json([
            'token'      => $token,
            'type'       => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ], 200);
    }

    public function createOrUpdateToken(Request $request, User $user, string $token)
    {
        $ip = $request->getClientIp();
        $user_code = $user->code;

        Token::updateOrCreate([
            'user_code' => $user_code,
            'ip'        => $ip
        ], [
            'token'     => $token
        ]);
    }
}

<?php

namespace App\Http\Controllers\Shared\Helpers;

use Illuminate\Http\Response;

class CommonResponse
{
    /**
     * Return a response when a resource was not found in the database or
     * some resource, etc.
     *
     * @param string $key
     * @return Response
     */
    public static function resourceNotFound(string $key = 'api.resource')
    {
        return response()->json([
            'message' => trans('api.invalid_resource', [
                'resource' => trans($key)
            ])
        ], 404);
    }

    /**
     * Return a response when someone is trying to access a resource which
     * is not directly related to.
     *
     * @return Response
     */
    public static function missingPermissions()
    {
        return response()->json([
            'message' => trans('api.missing_permissions')
        ], 401);
    }

    /**
     * Return a successfull response with a status of 200.
     *
     * @return Response
     */
    public static function success()
    {
        return response()->json([
            'success' => true
        ], 200);
    }
}

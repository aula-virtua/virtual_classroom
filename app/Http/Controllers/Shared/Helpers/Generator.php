<?php

namespace App\Http\Controllers\Shared\Helpers;

use App\Models\Course;
use App\Models\User;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

/**
 * @author Eduardo Fuentes <eduardo.fuentes.rangel@gmail.com>
 */
class Generator
{

    /**
     * Generate a random token for example to reset the password from an
     * account or for activation.
     *
     * @return string
     */
    public static function token()
    {
        return str_replace('-', '', Uuid::uuid4()->toString());
    }

    /**
     * Generates a random string that will work as the course code for a given
     * user.
     *
     * @return string
     */
    public static function courseCode()
    {
        $code = '';

        while (true) {
            $code = Str::random();

            if (Course::where('code', $code)->count() === 0) {
                break;
            }
        }

        return $code;
    }

    /**
     * Pick a random image for a banner for a course.
     *
     * @return string
     */
    public static function banner()
    {
        $banners = [
            'banner_0001.jpg',
            'banner_0002.jpg',
            'banner_0003.jpg',
            'banner_0004.jpg',
            'banner_0005.jpg',
            'banner_0006.jpg',
            'banner_0007.jpg',
            'banner_0008.jpg',
            'banner_0009.jpg',
            'banner_0010.jpg',
            'banner_0011.jpg',
            'banner_0012.jpg',
            'banner_0013.jpg',
            'banner_0014.jpg',
            'banner_0015.jpg',
            'banner_0016.jpg',
            'banner_0017.jpg',
            'banner_0018.jpg',
            'banner_0019.jpg',
            'banner_0020.jpg',
            'banner_0021.jpg',
            'banner_0022.jpg',
        ];

        $index = rand(0, count($banners) - 1);

        return $banners[$index];
    }
}

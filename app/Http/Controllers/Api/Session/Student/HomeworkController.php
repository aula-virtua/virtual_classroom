<?php

namespace App\Http\Controllers\Api\Session\Student;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Shared\Helpers\Generator;
use App\Http\Requests\UploadFilesRequest;
use App\Http\Resources\Session\Student\HomeworkDetailResource;
use App\Http\Resources\Session\Student\HomeworkResource;
use App\Models\Homework;
use App\Models\HomeworkFile;
use App\Models\HomeworkUpload;
use Illuminate\Http\Request;

class HomeworkController extends Controller
{
    public function index(Request $request, $id)
    {
        $user = $request->user();
        $per_page = $request->per_page ??  10;

        $homeworks = Homework::where('course_id', $id)
            ->paginate($per_page);

        $homeworks->getCollection()->transform(function ($h) use ($user) {
            $entry = HomeworkUpload::where('homework_id', $h->id)
                ->where('user_code', $user->code)
                ->first();

            $h->delivered = $entry !== null;

            return $h;
        });

        return HomeworkResource::collection($homeworks);
    }

    public function show(Request $request, $id, $hid)
    {
        $user = $request->user();
        $upload = HomeworkUpload::where('homework_id', $hid)
            ->where('user_code', $user->code)
            ->first();

        if ($upload) {
            $uploads = HomeworkFile::where('homework_id', $hid)
                ->where('user_code', $user->code)
                ->get();
            $upload->files = $uploads;
        }

        $homework = Homework::find($hid)->load('course.teacher');
        $files = HomeworkFile::where('homework_id', $hid)
            ->where('user_code', $homework->course->teacher->code)
            ->get();
        $homework->files = $files;
        $homework->upload = $upload;

        return (new HomeworkDetailResource($homework))
            ->response()
            ->setStatusCode(200);
    }

    public function store(UploadFilesRequest $request, $id, $hid)
    {
        $user = $request->user();

        if ($request->hasFile('files')) {
            $files = $request->file('files');
            foreach ($files as $file) {
                $extension = $file->getClientOriginalExtension();
                $name = Generator::token() . '.' . $extension;
                $file->storeAs(
                    sprintf('%s/%s/%s', $id, $hid, $user->code),
                    $name,
                    'homework_files'
                );

                HomeworkFile::create([
                    'homework_id'   => $hid,
                    'user_code'     => $user->code,
                    'mime_type'     => $file->getMimeType(),
                    'original_name' => $file->getClientOriginalName(),
                    'file_name'     => $name
                ]);
            }
        }

        if ($request->remove) {
            foreach ($request->remove as $file_name) {
                HomeworkFile::where('file_name', $file_name)
                    ->where('homework_id', $hid)
                    ->where('user_code', $user->code)
                    ->delete();
            }
        }

        HomeworkUpload::updateOrCreate([
            'homework_id' => $hid,
            'user_code'   => $user->code
        ], []);

        $upload = HomeworkUpload::where('homework_id', $hid)
            ->where('user_code', $user->code)
            ->first();

        if ($upload) {
            $uploads = HomeworkFile::where('homework_id', $hid)
                ->where('user_code', $user->code)
                ->get();
            $upload->files = $uploads;
        }

        $homework = Homework::find($hid)->load('course.teacher');
        $files = HomeworkFile::where('homework_id', $hid)
            ->where('user_code', $homework->course->teacher->code)
            ->get();
        $homework->files = $files;
        $homework->upload = $upload;

        return new HomeworkDetailResource($homework);
    }
}

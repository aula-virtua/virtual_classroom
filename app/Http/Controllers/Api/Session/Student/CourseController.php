<?php

namespace App\Http\Controllers\Api\Session\Student;

use App\Http\Controllers\Controller;
use App\Http\Resources\Session\Student\CourseDetailResource;
use App\Http\Resources\Session\Student\CourseResource;
use App\Models\Course;
use App\Models\CourseStudent;
use App\Models\HomeworkUpload;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function index(Request $request)
    {
        $per_page = $request->per_page ?? 10;

        $user = $request->user();

        $courses = CourseStudent::where('student_code', $user->code)
            ->with(['course.teacher', 'course.homeworks'])
            ->paginate($per_page);

        $courses->getCollection()->transform(function ($relation) use ($user) {
            $homeworkIds = $relation->course->homeworks()->pluck('id')->toArray();
            $homeworks = count($homeworkIds);
            $uploads = HomeworkUpload::where('user_code', $user->code)
                ->whereIn('homework_id', $homeworkIds)->count();

            $relation->course->difference = $homeworks - $uploads;

            return $relation->course;
        });

        return CourseResource::collection($courses);
    }

    public function show(Request $request, $id)
    {
        $user = $request->user();
        $course = Course::find($id);
        $homeworkIds = $course->homeworks()->pluck('id')->toArray();
        $homeworks = count($homeworkIds);
        $uploads = HomeworkUpload::where('user_code', $user->code)
            ->whereIn('homework_id', $homeworkIds)->count();

        $course->difference = $homeworks - $uploads;
        $course->calification = CourseStudent::where('course_id', $id)
            ->where('student_code', $user->code)->first()->calification;

        return (new CourseDetailResource($course))
            ->response()
            ->setStatusCode(200);
    }
}

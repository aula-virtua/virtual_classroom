<?php

namespace App\Http\Controllers\Api\Session\Student;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Shared\Helpers\CommonResponse;
use App\Models\Course;
use App\Models\CourseStudent;
use Illuminate\Http\Request;

class JoinController extends Controller
{
    public function join(Request $request, $code)
    {
        $course = Course::where('code', $code)->first();

        if (!$course) {
            return CommonResponse::resourceNotFound('api.course');
        }

        $user = $request->user();

        $relation = CourseStudent::where('course_id', $course->id)
            ->where('student_code', $user->code)
            ->first();

        if ($relation) {
            return response()->json([
                'message' => trans('api.joined')
            ], 403);
        }

        CourseStudent::create([
            'course_id'    => $course->id,
            'student_code' => $user->code
        ]);

        return CommonResponse::success();
    }

    public function leave(Request $request, $id)
    {
        $course = Course::find($id);

        if (!$course) {
            return CommonResponse::resourceNotFound('api.course');
        }

        $user = $request->user();

        $relation =  CourseStudent::where('course_id', $course->id)
            ->where('student_code', $user->code)
            ->first();

        if (!$relation) {
            return response()->json([
                'message' => trans('api.invalid_leave')
            ], 403);
        }

        $relation->forceDelete();

        return CommonResponse::success();
    }
}

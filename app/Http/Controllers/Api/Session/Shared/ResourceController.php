<?php

namespace App\Http\Controllers\Api\Session\Shared;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ResourceController extends Controller
{
    public function index(Request $request)
    {
        $path = '';

        $type     = $request->type;
        $fileName = $request->fileName;

        if ($type === 'file') {
            $course   = $request->course;
            $homework = $request->homework;
            $who      = $request->who;
            $path = sprintf('homework_files\%s\%s\%s\%s', $course, $homework, $who, $fileName);
        } else {
            $who      = $request->who;
            $path = sprintf('app\public\%s\%s', $who, $fileName);
        }

        return response()->file(storage_path($path));
    }
}

<?php

namespace App\Http\Controllers\Api\Session\Shared;

use App\Http\Controllers\Controller;
use App\Http\Resources\Session\Shared\ForumResource;
use App\Models\Course;
use Illuminate\Http\Request;

class ForumController extends Controller
{
    public function index(Request $request, $id)
    {
        $per_page = $request->per_page ?? 10;

        $forums = Course::find($id)->forums()->paginate($per_page);

        return ForumResource::collection($forums);
    }
}

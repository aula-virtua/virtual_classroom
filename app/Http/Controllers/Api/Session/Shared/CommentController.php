<?php

namespace App\Http\Controllers\Api\Session\Shared;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Shared\Helpers\CommonResponse;
use App\Http\Requests\CreateCommentRequest;
use App\Http\Resources\Session\Shared\CommentResource;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index(Request $request, $id, $fid, $cid = null)
    {
        $per_page = $request->per_page ?? 10;

        $comments = Comment::where('forum_id', $fid)
            ->with('user')
            ->where('comment_id', $cid)
            ->paginate($per_page);

        return CommentResource::collection($comments);
    }

    public function store(CreateCommentRequest $request, $id, $fid, $cid = null)
    {
        $user = $request->user();
        $formData = $request->only(['title', 'description']);

        $comment = new Comment($formData);
        $comment->forum_id = $fid;
        $comment->user_code = $user->code;
        $comment->comment_id = $cid;

        $comment->save();
        $comment->load(['user']);

        return new CommentResource($comment);
    }

    public function update(CreateCommentRequest $request, $id, $fid, $cid)
    {
        $formData = $request->validated();

        $comment = Comment::find($cid);
        $comment->update($formData);

        return (new CommentResource($comment))
            ->response()
            ->setStatusCode(200);
    }

    public function destroy(Request $request, $id, $fid, $cid)
    {
        Comment::find($cid)->delete();

        return CommonResponse::success();
    }
}

<?php

namespace App\Http\Controllers\Api\Session\Shared;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Shared\Helpers\Generator;
use App\Http\Controllers\Shared\Helpers\WithToken;
use App\Http\Requests\UpdatePasswordRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Resources\Session\Shared\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    use WithToken;

    public function index(Request $request)
    {
        $user = $request->user();

        return (new UserResource($user))
            ->response()
            ->setStatusCode(200);
    }

    public function logout(Request $request)
    {
        $user = $request->user();
        $this->createOrUpdateToken($request, $user, '');
        auth('api')->logout();

        return response()->json([
            'success' => true
        ]);
    }

    public function update(UpdateProfileRequest $request)
    {
        $user = $request->user();

        $formData = $request->validated();

        if ($request->hasFile('profile_image')) {
            $file = $request->file('profile_image');
            $extension = $file->getClientOriginalExtension();
            $name = Generator::token() . '.' . $extension;

            $file->storeAs(((string) $user->code), $name, 'public');
            $formData['profile_image'] = $name;
        }

        $user->update($formData);

        return (new UserResource($user))
            ->response()
            ->setStatusCode(200);
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        $user = $request->user();

        if (!Hash::check($request->current_password, $user->password)) {
            return response()->json([
                'errors' => [
                    'current_password' => [trans('api.current_password')]
                ]
            ], 422);
        }

        $user->password = $request->password;
        $user->save();

        return response()->json([
            'success' => true
        ], 200);
    }
}

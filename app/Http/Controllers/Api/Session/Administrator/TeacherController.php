<?php

namespace App\Http\Controllers\Api\Session\Administrator;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Shared\Helpers\CommonResponse;
use App\Http\Controllers\Shared\Helpers\Generator;
use App\Http\Requests\RegisterTeacherRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\Session\Administrator\TeacherDetailResource;
use App\Http\Resources\Session\Shared\UserResource;
use App\Models\Teacher;
use App\Notifications\TeacherWelcomeNotification;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;

class TeacherController extends Controller
{
    /**
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $per_page = $request->per_page ?? 10;

        $teachers = Teacher::withTrashed()
            ->paginate($per_page);

        return UserResource::collection($teachers);
    }

    public function show(Request $request, $code)
    {
        $teacher = Teacher::withTrashed()
            ->find($code);

        if (!$teacher) {
            return CommonResponse::resourceNotFound('api.teacher');
        }

        $teacher->load([
            'courses'
        ]);

        return (new TeacherDetailResource($teacher))
            ->response()
            ->setStatusCode(200);
    }

    /**
     *
     * @param Request $request
     * @return Response
     */
    public function store(RegisterTeacherRequest $request)
    {
        $formData = $request->only([
            'name',
            'first_last_name',
            'second_last_name',
            'code',
            'email'
        ]);

        $teacher = new Teacher($formData);
        $teacher->password = '';
        $teacher->activation_status = 1;
        $teacher->activation_token = '';
        $teacher->reset_token = Generator::token();
        $teacher->reset_token_created_at = Carbon::now();
        $teacher->save();
        $teacher->notify(new TeacherWelcomeNotification($teacher));

        return new UserResource($teacher);
    }

    /**
     * @param Request $request
     * @param Teacher $teacher
     * @return Response
     */
    public function update(UpdateUserRequest $request, $code)
    {
        $teacher = Teacher::withTrashed()->find($code);

        if (!$teacher) {
            return CommonResponse::resourceNotFound('api.teacher');
        }

        $teacher->update($request->validated());

        if (is_bool($request->activate) && $request->activate) {
            $teacher->restore();
        }

        return (new UserResource($teacher))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function destroy(Request $request, $code)
    {
        $teacher = Teacher::find($code);

        if (!$teacher) {
            return CommonResponse::resourceNotFound('api.teacher');
        }

        $teacher->delete();

        return response()->json(['success' => true], 200);
    }
}

<?php

namespace App\Http\Controllers\Api\Session\Administrator;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Shared\Helpers\CommonResponse;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\Session\Shared\UserResource;
use App\Models\Student;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StudentController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $per_page = $request->per_page ?? 10;

        $students = Student::withTrashed()
            ->paginate($per_page);

        return UserResource::collection($students);
    }

    /**
     * @param UpdateUserRequest $request
     * @param string $code
     * @return Response
     */
    public function update(UpdateUserRequest $request, $code)
    {
        $student = Student::withTrashed()
            ->find($code);

        if (!$student) {
            return CommonResponse::resourceNotFound('api.student');
        }

        $student->update($request->validated());

        if (is_bool($request->activate) && $request->activate) {
            $student->restore();
        }

        return (new UserResource($student))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * @param Request $request
     * @param string  $code
     * @return Response
     */
    public function destroy(Request $request, $code)
    {
        $student = Student::find($code);

        if (!$student) {
            return CommonResponse::resourceNotFound('api.student');
        }

        $student->delete();

        return CommonResponse::success();
    }
}

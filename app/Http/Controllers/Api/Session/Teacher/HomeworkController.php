<?php

namespace App\Http\Controllers\Api\Session\Teacher;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Shared\Helpers\CommonResponse;
use App\Http\Controllers\Shared\Helpers\Generator;
use App\Http\Requests\CreateHomeworkRequest;
use App\Http\Requests\UpdateHomeworkRequest;
use App\Http\Resources\Session\Teacher\HomeworkDetailResource;
use App\Http\Resources\Session\Teacher\HomeworkResource;
use App\Models\Course;
use App\Models\Homework;
use App\Models\HomeworkFile;
use Illuminate\Http\Request;

class HomeworkController extends Controller
{
    public function index(Request $request, $id)
    {
        $per_page = $request->per_page ?? 10;

        $homeworks = Course::find($id)->homeworks()->paginate($per_page);

        return HomeworkResource::collection($homeworks);
    }

    public function store(CreateHomeworkRequest $request, $id)
    {
        $formData = $request->only([
            'name',
            'description',
            'max_calification',
            'delivery_date'
        ]);

        $homework = new Homework($formData);
        $homework->course_id = $id;
        $homework->save();

        $user = $request->user();

        if ($request->hasFile('files')) {
            $files = $request->file('files');
            foreach ($files as $file) {
                $extension = $file->getClientOriginalExtension();
                $name = Generator::token() . '.' . $extension;
                $file->storeAs(
                    sprintf('%s/%s/%s', $id, $homework->id, $user->code),
                    $name,
                    'homework_files'
                );

                HomeworkFile::create([
                    'homework_id'   => $homework->id,
                    'user_code'     => $user->code,
                    'mime_type'     => $file->getMimeType(),
                    'original_name' => $file->getClientOriginalName(),
                    'file_name'     => $name
                ]);
            }
        }

        $files = HomeworkFile::where('homework_id', $homework->id)
            ->where('user_code', $user->code)
            ->get();
        $homework->files = $files;

        return new HomeworkDetailResource($homework);
    }

    public function show(Request $request, $id, $hid)
    {
        $user = $request->user();

        $files = HomeworkFile::withTrashed()
            ->where('homework_id', $hid)
            ->where('user_code', $user->code)
            ->get();

        $homework = Homework::withTrashed()->find($hid);
        $homework->files = $files;

        return (new HomeworkDetailResource($homework))
            ->response()
            ->setStatusCode(200);
    }

    public function update(UpdateHomeworkRequest $request, $id, $hid)
    {
        $homework = Homework::withTrashed()->find($hid);

        $formData = $request->validated();
        unset($formData['files']);
        unset($formData['remove']);
        unset($formData['activate']);
        $user = $request->user();

        if ($request->hasFile('files')) {
            $files = $request->file('files');
            foreach ($files as $file) {
                $extension = $file->getClientOriginalExtension();
                $name = Generator::token() . '.' . $extension;
                $file->storeAs(
                    sprintf('%s/%s/%s', $id, $homework->id, $user->code),
                    $name,
                    'homework_files'
                );

                HomeworkFile::create([
                    'homework_id'   => $homework->id,
                    'user_code'     => $user->code,
                    'mime_type'     => $file->getMimeType(),
                    'original_name' => $file->getClientOriginalName(),
                    'file_name'     => $name
                ]);
            }
        }

        if ($request->remove) {
            foreach ($request->remove as $file_name) {
                HomeworkFile::where('file_name', $file_name)
                    ->where('homework_id', $hid)
                    ->where('user_code', $user->code)
                    ->delete();
            }
        }

        if (is_bool($request->activate) && $request->activate) {
            $homework->restore();
        }

        $files = HomeworkFile::where('homework_id', $hid)
            ->where('user_code', $user->code)
            ->get();
        $homework->update($formData);
        $homework->files = $files;

        return (new HomeworkDetailResource($homework))
            ->response()
            ->setStatusCode(200);
    }

    public function destroy(Request $request, $id, $hid)
    {
        Homework::find($hid)->delete();

        return CommonResponse::success();
    }
}

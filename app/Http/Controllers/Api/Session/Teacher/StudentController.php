<?php

namespace App\Http\Controllers\Api\Session\Teacher;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Shared\Helpers\CommonResponse;
use App\Http\Resources\Session\Student\HomeworkUploadResource;
use App\Http\Resources\Session\Teacher\StudentResource;
use App\Models\Course;
use App\Models\Homework;
use App\Models\HomeworkFile;
use App\Models\HomeworkUpload;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index(Request $request, $id)
    {
        $course = Course::find($id);
        $course->load(['students']);

        return StudentResource::collection($course->students);
    }

    public function show(Request $request, $id, $hid, $code)
    {
        $upload = HomeworkUpload::where('user_code', $code)
            ->where('homework_id', $hid)
            ->first();

        $files = HomeworkFile::where('homework_id', $hid)
            ->where('user_code', $code)
            ->get();

        $upload->files = $files;

        return (new HomeworkUploadResource($upload))
            ->response()
            ->setStatusCode(200);
    }

    public function update(Request $request, $id, $hid, $code)
    {
        $homework = Homework::find($hid);

        if ($request->calification > $homework->max_calification) {
            return response()->json([
                'message' => trans('api.calification')
            ], 422);
        }

        HomeworkUpload::updateOrCreate([
            'homework_id' => $hid,
            'user_code'   => $code
        ], [
            'calification' => $request->calification
        ]);

        return CommonResponse::success();
    }
}

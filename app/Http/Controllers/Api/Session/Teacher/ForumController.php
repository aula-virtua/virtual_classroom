<?php

namespace App\Http\Controllers\Api\Session\Teacher;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Shared\Helpers\CommonResponse;
use App\Http\Requests\CreateForumRequest;
use App\Http\Requests\UpdateForumRequest;
use App\Http\Resources\Session\Shared\ForumResource;
use App\Models\Forum;
use Illuminate\Http\Request;

class ForumController extends Controller
{
    public function store(CreateForumRequest $request, $id)
    {
        $formData = $request->only([
            'title',
            'description'
        ]);

        $forum = new Forum($formData);
        $forum->course_id = $id;
        $forum->save();

        return new ForumResource($forum);
    }

    public function update(UpdateForumRequest $request, $id, $fid)
    {
        $forum = Forum::withTrashed()->find($fid);

        $formData = $request->validated();
        unset($formData['activate']);

        if (is_bool($request->activate) && $request->activate) {
            $forum->restore();
        }

        $forum->update($formData);

        return (new ForumResource($forum))
            ->response()
            ->setStatusCode(200);
    }

    public function destroy(Request $request, $id, $fid)
    {
        Forum::find($fid)->delete();

        return CommonResponse::success();
    }
}

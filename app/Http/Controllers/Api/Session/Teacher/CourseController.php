<?php

namespace App\Http\Controllers\Api\Session\Teacher;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Shared\Helpers\CommonResponse;
use App\Http\Controllers\Shared\Helpers\Generator;
use App\Http\Requests\CreateCourseRequest;
use App\Http\Resources\Session\Teacher\CourseDetailResourse;
use App\Http\Resources\Session\Teacher\CourseResource;
use App\Models\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function index(Request $request)
    {
        $per_page = $request->per_page ?? 10;

        $courses = Course::withTrashed()
            ->where('teacher_code', $request->user()->code)
            ->paginate($per_page);

        return CourseResource::collection($courses);
    }

    public function show(Request $request, $id)
    {
        $course = Course::find($id);
        // TODO: Extra Fields

        return (new CourseDetailResourse($course))
            ->response()
            ->setStatusCode(200);
    }

    public function store(CreateCourseRequest $request)
    {
        $formData = $request->only([
            'name',
            'description',
            'code'
        ]);

        $user = $request->user();
        $course = new Course($formData);
        $course->teacher_code = $user->code;
        $course->banner = Generator::banner();
        $course->save();

        return new CourseResource($course);
    }

    public function update(CreateCourseRequest $request, $id)
    {
        $course = Course::withTrashed()->find($id);

        $course->update($request->validated());

        return (new CourseResource($course))
            ->response()
            ->setStatusCode(200);
    }

    public function destroy(Request $request, $id)
    {
        Course::find($id)->delete();

        return CommonResponse::success();
    }
}

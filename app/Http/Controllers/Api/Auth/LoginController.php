<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Shared\Helpers\WithToken;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * The next controller will handle all the login for
 * when the user wants to sign into the application.
 *
 * @author Eduardo Fuentes <eduardo.fuentes.rangel@gmail.com>
 */
class LoginController extends Controller
{
    use WithToken;

    public function login(LoginRequest $request)
    {
        $user = User::withTrashed()
            ->where('code', $request->code)
            ->orWhere('email', $request->code)
            ->first();

        if (!$user) {
            return response()->json([
                'message' => trans('api.credentials')
            ], 401);
        }

        if (!Hash::check($request->password, $user->password)) {
            return response()->json([
                'message' => trans('api.credentials')
            ], 401);
        }

        if ($user->trashed()) {
            return response()->json([
                'message' => trans('api.suspended')
            ], 401);
        }

        $token  = JWTAuth::fromUser($user);
        $this->createOrUpdateToken($request, $user, $token);

        return $this->responseWithToken($token);
    }
}

<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Shared\Helpers\Generator;
use App\Http\Requests\ForgotPasswordRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Models\User;
use App\Notifications\ForgotPasswordNotification;
use Illuminate\Support\Carbon;

/**
 * @author Eduardo Fuentes <eduardo.fuentes.rangel@gmail.com>
 */
class ForgotPasswordController extends Controller
{
    public function forgot(ForgotPasswordRequest $request)
    {
        $user = User::where('code', $request->code)
            ->orWhere('email', $request->code)
            ->first();

        if ($user) {
            $user->reset_token = Generator::token();
            $user->reset_token_created_at = Carbon::now();
            $user->save();

            $user->notify(new ForgotPasswordNotification($user));
        }

        return response()->json([
            'success' => true
        ], 200);
    }

    public function reset(ResetPasswordRequest $request)
    {
        $user = User::where('reset_token', $request->token)->first();

        if (!$user) {
            return response()->json([
                'message' => trans('api.reset_token')
            ], 400);
        }

        $createdAt = $user->reset_token_created_at;
        if (!Carbon::now()->lt($createdAt->addHours(12))) {
            $user->reset_token = '';
            $user->reset_token_created_at = null;
            $user->save();

            return response()->json([
                'message' => trans('api.reset_token_expired')
            ], 400);
        }

        $user->password = $request->password;
        $user->reset_token = '';
        $user->reset_token_created_at = null;
        $user->save();

        return response()->json([
            'success' => true
        ], 201);
    }
}

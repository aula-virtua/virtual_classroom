<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Shared\Helpers\Generator;
use App\Http\Requests\RegisterStudentRequest;
use App\Models\Student;
use App\Notifications\StudentWelcomeNotification;
use Illuminate\Http\JsonResponse;

/**
 * Controller to handle incoming requests to create new students in the
 * platform.
 *
 * @author Eduardo Fuentes <eduardo.fuentes.rangel@gmail.com>
 */
class RegisterStudentController extends Controller
{
    /**
     * Register a new student on the site.
     *
     * @param RegisterStudentRequest $request Form fields
     *
     * @return JsonResponse
     */
    public function register(RegisterStudentRequest $request)
    {
        $formData = $request->only([
            'name',
            'first_last_name',
            'second_last_name',
            'code',
            'name',
            'email'
        ]);

        $student = new Student($formData);
        $student->password = $request->password;
        $student->reset_token = '';
        $student->activation_token = Generator::token();
        $student->save();
        // $student->notify(new StudentWelcomeNotification($student));

        return response()->json([
            'success' => true
        ], 201);
    }
}

<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class ActivationController extends Controller
{
    public function verify(Request $request, string $token)
    {
        $user = User::where('activation_token', $token)->first();

        if (!$user) {
            return response()->json([
                'message' => trans('api.activation_token')
            ], 400);
        }

        $user->activation_token = '';
        $user->activation_status = true;
        $user->save();

        return response()->json([
            'success' => true
        ], 200);
    }
}

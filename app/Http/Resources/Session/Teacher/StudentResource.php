<?php

namespace App\Http\Resources\Session\Teacher;

use Illuminate\Http\Resources\Json\JsonResource;

class StudentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'code'           => $this->code,
            'name'           => $this->name,
            'email'          => $this->email,
            'firstLastName'  => $this->first_last_name,
            'secondLastName' => $this->second_last_name,
            'profileImage'   => $this->profile_image,
            'calification'   => $this->pivot->calification,
            'joinedAt'       => $this->pivot->created_at,
        ];
    }
}

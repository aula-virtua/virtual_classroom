<?php

namespace App\Http\Resources\Session\Administrator;

use Illuminate\Http\Resources\Json\JsonResource;

class TeacherDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'code'             => $this->code,
            'name'             => $this->name,
            'firstLastName'    => $this->first_last_name,
            'secondLastName'   => $this->second_last_name,
            'email'            => $this->email,
            'profileImage'     => $this->profile_image,
            'activationStatus' => $this->activation_status,
            'courses'          => CourseResource::collection($this->courses),
            'createdAt'        => $this->created_at,
            'updatedAt'        => $this->updated_at,
            'deletedAt'        => $this->deleted_at
        ];
    }
}

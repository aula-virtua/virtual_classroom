<?php

namespace App\Http\Resources\Session\Administrator;

use Illuminate\Http\Resources\Json\JsonResource;

class CourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'name'         => $this->name,
            'description'  => $this->description,
            'code'         => $this->code,
            'banner'       => $this->banner,
            'createdAt'    => $this->created_at,
            'updatedAt'    => $this->updated_at,
            'deletedAt'    => $this->deleted_at
        ];
    }
}

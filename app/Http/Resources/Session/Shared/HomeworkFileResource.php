<?php

namespace App\Http\Resources\Session\Shared;

use Illuminate\Http\Resources\Json\JsonResource;

class HomeworkFileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'fileName'     => $this->file_name,
            'originalName' => $this->original_name,
            'mimeType'     => $this->mime_type
        ];
    }
}

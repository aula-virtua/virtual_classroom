<?php

namespace App\Http\Resources\Session\Shared;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'code'             => $this->code,
            'name'             => $this->name,
            'firstLastName'    => $this->first_last_name,
            'secondLastName'   => $this->second_last_name,
            'email'            => $this->email,
            'profileImage'     => $this->profile_image,
            'activationStatus' => $this->activation_status,
            'createdAt'        => $this->created_at,
            'updatedAt'        => $this->updated_at,
            'deletedAt'        => $this->deleted_at
        ];
    }
}

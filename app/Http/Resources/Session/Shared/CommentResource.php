<?php

namespace App\Http\Resources\Session\Shared;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'title'       => $this->title,
            'description' => $this->description,
            'createdBy'   => [
                'code'           => $this->user->code,
                'name'           => $this->user->name,
                'firstLastName'   => $this->user->first_last_name,
                'secondLastName' => $this->user->second_last_name,
                'profileImage'   => $this->user->profile_image
            ],
            'createdAt'   => $this->created_at,
            'updatedAt'   => $this->updated_at,
            'deletedAt'   => $this->deleted_at,
        ];
    }
}

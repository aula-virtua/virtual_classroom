<?php

namespace App\Http\Resources\Session\Student;

use Illuminate\Http\Resources\Json\JsonResource;

class CourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'banner'      => $this->banner,
            'cdde'        => $this->code,
            'teacher'   => [
                'code'           => $this->teacher->code,
                'name'           => $this->teacher->name,
                'firstLastName'  => $this->teacher->first_last_name,
                'secondLastName' => $this->teacher->second_last_name,
                'email'          => $this->teacher->email,
            ],
            'difference' => $this->difference,
        ];
    }
}

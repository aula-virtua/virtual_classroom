<?php

namespace App\Http\Resources\Session\Student;

use App\Http\Resources\Session\Shared\HomeworkFileResource;
use Illuminate\Http\Resources\Json\JsonResource;

class HomeworkDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'              => $this->id,
            'name'            => $this->name,
            'description'     => $this->description,
            'deliveryDate'    => $this->delivery_date,
            'maxCalification' => $this->max_calification,
            'files'           => HomeworkFileResource::collection($this->files),
            'upload'          => $this->upload ? new HomeworkUploadResource($this->upload) : null,
            'createdAt'       => $this->created_at,
            'updatedAt'       => $this->updated_at,
            'deletedAt'       => $this->deleted_at
        ];
    }
}

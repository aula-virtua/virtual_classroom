<?php

namespace App\Http\Resources\Session\Student;

use App\Http\Resources\Session\Shared\HomeworkFileResource;
use Illuminate\Http\Resources\Json\JsonResource;

class HomeworkUploadResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'calification' => $this->calification,
            'files'        => HomeworkFileResource::collection($this->files),
            'uploadedAt'   => $this->created_at,
        ];
    }
}

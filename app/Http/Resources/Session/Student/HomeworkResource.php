<?php

namespace App\Http\Resources\Session\Student;

use Illuminate\Http\Resources\Json\JsonResource;

class HomeworkResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'name'         => $this->name,
            'description'  => $this->description,
            'deliveryDate' => $this->delivery_date,
            'status'       => $this->delivered,
            'createdAt'    => $this->created_at,
            'updatedAt'    => $this->updated_at,
            'deletedAt'    => $this->deleted_at
        ];
    }
}

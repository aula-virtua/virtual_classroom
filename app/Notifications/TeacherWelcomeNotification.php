<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TeacherWelcomeNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('reset/' . $notifiable->reset_token);

        return (new MailMessage)
            ->line('Bienvenido a ' . env('APP_NAME') . ' ' . $notifiable->name)
            ->line('Has sido agregedo por un administrador, se cuenta ha sido activada.')
            ->line('Para poder iniciar sesión, necesitas dar click sobre el botón que esta justo abajo para colocar tu contraseña.')
            ->action('Colocar Contraseña', $url)
            ->line('Es importante que lo hagas dentro de las siguientes 12 horas, de otra forma el token será inválido.')
            ->line('En caso de que este link caduque, puedes usar el formulario de Contraseña Olvidada para reestablecerla.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

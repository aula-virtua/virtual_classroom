<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ForgotPasswordNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('reset/' . $notifiable->reset_token);

        return (new MailMessage)
            ->line('Que tal ' . $notifiable->name . '!')
            ->line('Te enviamos este correo para que puedas cambiar tu contraseña.')
            ->action('Cambiar Contraseña', $url)
            ->line('Es importante que lo hagas dentro de las siguientes 12 horas, de otra forma el token será inválido')
            ->line('Si tu no pediste el cambio de contraseña, solo ignora este correo.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * Course Model
 *
 * @property int    $id
 * @property string $name
 * @property string $description
 * @property string $code
 * @property string $banner
 * @property int    $teacher_code
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @author Eduardo Fuentes <eduardo.fuentes.rangel@gmail.com>
 */
class Course extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'teacher_code', 'name',
        'description', 'code',
        'banner', 'teacher_code'
    ];

    /**
     * A course has many students
     *
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function students()
    {
        return $this->belongsToMany(User::class, CourseStudent::class, 'course_id', 'student_code')
            ->withPivot('calification')
            ->withTimestamps()
            ->select('name', 'first_last_name', 'second_last_name', 'email', 'code', 'profile_image');
    }

    /**
     * A course has many homeworks
     *
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function homeworks()
    {
        return $this->hasMany(Homework::class, 'course_id');
    }

    /**
     * A course has many homeworks
     *
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function forums()
    {
        return $this->hasMany(Forum::class, 'course_id');
    }

    /**
     * Creator of the course
     *
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function teacher()
    {
        return $this->belongsTo(User::class, 'teacher_code');
    }
}

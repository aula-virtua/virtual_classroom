<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Comment Model
 *
 * @property int    $id
 * @property int    $forum_id
 * @property int    $user_code
 * @property int    $comment_id
 * @property string $title
 * @property string $description
 *
 * @author Eduardo Fuentes <eduardo.fuentes.rangel@gmail.com>
 */
class Comment extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title', 'description',
        'forum_id', 'comment_id',
        'user_code'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_code', 'code');
    }
}

<?php

namespace App\Models;

use App\Http\Controllers\Shared\Helpers\HasCompositePrimaryKey;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HomeworkUpload extends Model
{
    use SoftDeletes, HasCompositePrimaryKey;

    protected $primaryKey = ['user_code', 'homework_id'];

    public $incrementing = false;

    protected $fillable = [
        'calification',
        'user_code',
        'homework_id'
    ];
}

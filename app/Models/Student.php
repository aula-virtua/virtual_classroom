<?php

namespace App\Models;

use App\Http\Controllers\Shared\Constants\UserType;

class Student extends User
{
    protected $table = 'users';

    protected $attributes = [
        'user_type' => '3'
    ];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(function ($query) {
            $query->where('user_type', UserType::STUDENT);
        });
    }
}

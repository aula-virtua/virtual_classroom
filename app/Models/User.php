<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * User Model that will serve as the base class for the specific user
 * implementattions.
 *
 * @property int    $code
 * @property string $name
 * @property string $first_last_name
 * @property string $second_last_name
 * @property string $email
 * @property int    $user_type
 * @property string $reset_token
 * @property Carbon $reset_token_created_at
 * @property string $profile_image
 * @property bool   $activation_status
 * @property string $activation_token
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @author Eduardo Fuentes <eduardo.fuentes.rangel@gmail.com>
 */
class User extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes;

    protected $primaryKey = 'code';

    public $incrementing = false;

    protected $dates = [
        'reset_token_created_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'name',
        'first_last_name',
        'second_last_name',
        'email', 'profile_image',
        'user_type', 'reset_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'reset_token',
    ];

    public function setPasswordAttribute(string $password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [
            'typ' => $this->user_type
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function courses()
    {
        return $this->hasMany(Course::class, 'teacher_code');
    }
}

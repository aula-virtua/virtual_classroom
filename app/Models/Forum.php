<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * Forum Model
 *
 * @property int    $id
 * @property string $title
 * @property string $description
 * @property int    $course_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @author Eduardo Fuentes <eduardo.fuentes.rangel@gmail.com>
 */
class Forum extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'course_id', 'title',
        'description'
    ];

    /**
     * Comments from a forum.
     *
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function comments()
    {
        return $this->hasMany(Comment::class, 'forum_id');
    }
}

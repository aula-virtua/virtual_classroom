<?php

namespace App\Models;

use App\Http\Controllers\Shared\Helpers\HasCompositePrimaryKey;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * Course Student Relationship
 *
 * @property int    $student_code
 * @property int    $course_id
 * @property int    $calification
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @author Eduardo Fuentes <eduardo.fuentes.rangel@gmail.com>
 */
class CourseStudent extends Model
{
    use SoftDeletes, HasCompositePrimaryKey;

    protected $primaryKey = ['course_id', 'student_code'];

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'student_code',
        'course_id',
        'calification'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }
}

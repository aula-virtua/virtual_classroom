<?php

namespace App\Models;

use App\Http\Controllers\Shared\Helpers\HasCompositePrimaryKey;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * Homework file Model
 *
 * @property int    $homework_id
 * @property int    $user_code
 * @property string $file_name
 * @property string $mime_type
 * @property string $original_name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @author Eduardo Fuentes <eduardo.fuentes.rangel@gmail.com>
 */
class HomeworkFile extends Model
{
    use SoftDeletes, HasCompositePrimaryKey;

    protected $primaryKey = ['homework_id', 'user_code'];

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'homework_id',
        'user_code',
        'file_name', 'original_name',
        'mime_type'
    ];
}

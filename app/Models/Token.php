<?php

namespace App\Models;

use App\Http\Controllers\Shared\Helpers\HasCompositePrimaryKey;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Token Model
 *
 * @property int    $user_code
 * @property string $ip
 * @property string $token
 *
 * @author Eduardo Fuentes <eduardo.fuentes.rangel@gmail.com>
 */
class Token extends Model
{
    use HasCompositePrimaryKey, SoftDeletes;

    protected $primaryKey = ['user_code', 'ip'];

    public $incrementing = false;

    protected $fillable = [
        'user_code', 'ip',
        'token'
    ];
}

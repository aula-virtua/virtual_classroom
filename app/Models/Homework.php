<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * Homework Model
 *
 * @property int    $id
 * @property string $name
 * @property string $description
 * @property Carbon $delivery_date
 * @property int    $max_calification
 * @property int    $course_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @author Eduardo Fuentes <eduardo.fuentes.rangel@gmail.com>
 */
class Homework extends Model
{
    use SoftDeletes;

    protected $dates = [
        'delivey_date',
    ];

    protected $fillable = [
        'name', 'description',
        'max_calification',
        'delivery_date'
    ];

    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }
}

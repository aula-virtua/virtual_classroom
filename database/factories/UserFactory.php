<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'code'             => $faker->randomNumber(9),
        'name'             => $faker->name,
        'first_last_name'  => $faker->lastName,
        'second_last_name' => $faker->lastName,
        'password'         => '12345',
        'email'            => $faker->email,
        'reset_token'      => ''
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Http\Controllers\Shared\Helpers\Generator;
use App\Models\HomeworkFile;
use Faker\Generator as Faker;

$factory->define(HomeworkFile::class, function (Faker $faker) {
    return [
        'file_name'     => Generator::token() . '.png',
        'original_name' => $faker->name . '.png',
        'mime_type'     => 'image/png',
    ];
});

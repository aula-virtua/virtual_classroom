<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Http\Controllers\Shared\Helpers\Generator;
use App\Models\Course;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'name'          => $faker->bs,
        'description'   => $faker->sentence(20),
        'code'          => Str::random(12),
        'banner'        => Generator::banner(),
        'teacher_code'  => MainUsersSeeder::$users['teacher']['code'],
    ];
});

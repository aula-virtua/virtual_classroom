<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Homework;
use Faker\Generator as Faker;
use Illuminate\Support\Carbon;

$factory->define(Homework::class, function (Faker $faker) {
    return [
        'name'             => $faker->bs,
        'description'      => $faker->sentence(120),
        'max_calification' => $faker->numberBetween(50, 100),
        'delivery_date'    => Carbon::now()->addDay()
    ];
});

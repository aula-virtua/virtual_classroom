<?php

use App\Models\Course;
use App\Models\Forum;
use App\Models\Homework;
use App\Models\HomeworkFile;
use App\Models\HomeworkUpload;
use Illuminate\Database\Seeder;

class CourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teacherCode = MainUsersSeeder::$users['teacher']['code'];
        $code = MainUsersSeeder::$users['student']['code'];
        $courses = factory(Course::class, rand(3, 10))->create();
        $courses->each(function ($course) use ($code, $teacherCode) {
            $course->students()->attach($code);

            factory(Homework::class, rand(3, 7))->create([
                'course_id' => $course->id
            ])->each(function ($homework) use ($code, $teacherCode) {
                $p = rand(1, 10);

                if ($p > 5) {
                    HomeworkUpload::create([
                        'homework_id'  => $homework->id,
                        'user_code'    => $code,
                        'calification' => 0
                    ]);
                }

                factory(HomeworkFile::class, rand(4, 7))->create([
                    'homework_id' => $homework->id,
                    'user_code'   => $teacherCode
                ]);
            });

            factory(Forum::class, rand(3, 5))->create([
                'course_id' => $course->id
            ]);
        });
    }
}

<?php

use App\Http\Controllers\Shared\Helpers\Generator;
use App\Models\User;
use Illuminate\Database\Seeder;

/**
 * This seeder will take care of creating the main users for our application
 * for testing purposes. It's important to notice that the password from the
 * different users
 *
 * @author Eduardo Fuentes <eduardo.fuentes.rangel@gmail.com>
 */
class MainUsersSeeder extends Seeder
{
    /**
     * This variable contains the one user of each type of rol that the platform
     * will support
     *
     * @var array
     */
    public static $users = [
        'administrator' => [
            'user_type'        => '1',
            'code'             => 218746956,
            'name'             => 'Eduardo',
            'first_last_name'  => 'Fuentes',
            'second_last_name' => 'Rangel',
            'password'         => 'admin123',
            'email'            => 'eduardo.fuentes.rangel@gmail.com'
        ],
        'teacher' => [
            'user_type'        => '2',
            'code'             => 1234567890,
            'name'             => 'Carlos Dario',
            'first_last_name'  => 'Arenas',
            'second_last_name' => 'Yerena',
            'password'         => 'teacher123',
            'email'            => 'wero.fuentes.rangel@gmail.com'
        ],
        'student' => [
            'user_type'        => '3',
            'code'             => 215419156,
            'name'             => 'Esau',
            'first_last_name'  => 'López',
            'second_last_name' => 'Barajas',
            'password'         => 'student123',
            'email'            => 'jeduardo.fuentes@alumnos.udg.mx'
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (static::$users as $user) {
            $u = new User($user);
            $u->password = $user['password'];
            $u->reset_token = '';
            $u->activation_token = Generator::token();
            $u->save();
        }
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeworkFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homework_files', function (Blueprint $table) {
            $table->unsignedBigInteger('homework_id');
            $table->unsignedBigInteger('user_code');
            $table->string('file_name', 120);
            $table->string('mime_type', 40);
            $table->string('original_name', 200);
            $table->primary(['file_name', 'homework_id', 'user_code']);
            $table->foreign('homework_id')->references('id')->on('homeworks');
            $table->foreign('user_code')->references('code')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homework_files');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->unsignedBigInteger('code');
            $table->string('name', 40);
            $table->string('first_last_name', 40);
            $table->string('second_last_name', 40);
            $table->string('password', 130);
            $table->string('email', 50)->unique();
            $table->enum('user_type', array(1, 2, 3))->default(3);
            $table->string('reset_token', 130);
            $table->timestamp('reset_token_created_at')->nullable();
            $table->string('profile_image', 100)->nullable();
            $table->boolean('activation_status')->default(false);
            $table->string('activation_token')->default('');
            $table->primary('code');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

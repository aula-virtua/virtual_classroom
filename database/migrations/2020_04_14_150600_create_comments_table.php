<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('forum_id');
            $table->unsignedBigInteger('user_code');
            $table->unsignedBigInteger('comment_id')->nullable();
            $table->string('title', 60)->default('');
            $table->longText('description');
            $table->foreign('forum_id')->references('id')->on('forums');
            $table->foreign('user_code')->references('code')->on('users');
            $table->foreign('comment_id')->references('id')->on('comments');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeworkUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homework_uploads', function (Blueprint $table) {
            $table->unsignedBigInteger('homework_id');
            $table->unsignedBigInteger('user_code');
            $table->unsignedTinyInteger('calification');
            $table->primary(['homework_id', 'user_code']);
            $table->foreign('homework_id')->references('id')->on('homeworks');
            $table->foreign('user_code')->references('code')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homework_uploads');
    }
}

/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

import './bootstrap'
import Vue from 'vue'
import router from './router'
import App from './views/App.vue'

new Vue({
  router,
  el: '#app',
  render: h => h(App)
})

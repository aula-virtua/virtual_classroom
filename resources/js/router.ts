import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from './views/Home.vue'
import LoginView from './views/Login.vue'
import RegisterView from './views/Register.vue'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: HomeView
    },
    {
      path: '/sign-in',
      component: LoginView
    },
    {
      path: '/register',
      component: RegisterView
    }
  ]
})

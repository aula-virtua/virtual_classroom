<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Virtual Classroom</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&family=Raleway&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>

<body>
    <main id="app"></main>
    <script src="{{asset('js/app.js')}}"></script>
</body>

</html>

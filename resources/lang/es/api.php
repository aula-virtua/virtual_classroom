<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Custom Language Lines for API
    |--------------------------------------------------------------------------
    */

    'credentials' => 'Credenciales Inválidas.',
    'unauthorized' => 'Acceso Denegedo. Probablemente no tienes suficientes permisos para usar: :url',
    'blacklisted' => 'El token proveído ya no es válido.',
    'activation_token' => 'El token de activación no es válido.',
    'reset_token' => 'El token para cambiar la contraseña es inválido.',
    'reset_token_expired' => 'El token para cambiar la contraseña ha expirado.',
    'current_password' => 'La contraseña actual no es correcta.',
    'suspended' => 'Cuenta se encuentra suspendida.',
    'invalid_resource' => 'El :resource no fue encontrado.',
    'teacher' => 'maestro',
    'student' => 'estudiante',
    'resource' => 'recurso',
    'homework' => 'tarea',
    'course' => 'curso',
    'forum' => 'foro',
    'comment' => 'comment',
    'missing_permissions' => 'No tienes suficientes permisos para acceder al recurso.',
    'joined' => 'Ya te encuentras registrado a este curso.',
    'invalid_leave' => 'No te encuentras registrado a este curso.',
    'calification' => 'La calificación no puede ser mayor que la calificación máxima asignada a la tarea.'

];

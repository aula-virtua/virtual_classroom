module.exports = {
  theme: {
    fontFamily: {
      main: ['"Raleway"'],
      secondary: ['"Roboto"']
    },
    extend: {
      colors: {
        primary: '#04baf2',
        secondary: '#48ccf5',
        accent: '#eaf4fc',
        'light-gray': '#ada7a9',
        'dark-gray': '#7f7578'
      },
      borderRadius: {
        default: '24px'
      }
    }
  },
  variants: {},
  plugins: []
}

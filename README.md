# Virtual Classroom

## Requirements

In order to run the project we need to have install the next things:

- [Composer](https://router.vuejs.org/)
- [Node & NPM](https://nodejs.org/en/)

## Installation

### PHP

To install the dependencies for the Laravel Framework we will need to run:

```sh
composer install
```

And after that, we need to create the `.env` file that will have the basic
configuration for the Laravel App. To do that we will run:

```sh
cp .env.example .env
```

Next, we will set the key for our application with:

```sh
php artisan key:generate
```

And finally, the rest is to set the proper configuration for our database which
can be found in the `.env` file that we previously created.

### JS

To get our front-end up and running we need to run:

```sh
npm install
```

And to start our development environment we can do:

```
npm run watch
```
